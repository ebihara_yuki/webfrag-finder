package com.wittified.fragfinder.ao;

import net.java.ao.Entity;

/**
 * Created by daniel on 11/10/14.
 */
public interface CapturedEventParam extends Entity
{
    public String getName();
    public void setName( String name);

    public String getValue();
    public void setValue( String value );

    public void setCapturedEvent( CapturedEvent capturedEvent);
    public CapturedEvent getCapturedEvent();

}
