package com.wittified.fragfinder.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;

import java.util.Date;

/**
 * Created by daniel on 11/10/14.
 */
public interface CapturedEvent extends Entity
{
    public void setName( String name);
    public String getName();

    public Date getTime();
    public void setTime( Date date);

    @OneToMany
    public CapturedEventParam[] getCapturedEventParams();
}
