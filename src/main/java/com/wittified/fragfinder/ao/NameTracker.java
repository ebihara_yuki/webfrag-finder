package com.wittified.fragfinder.ao;

import net.java.ao.Entity;

/**
 * Created by daniel on 3/27/15.
 */
public interface NameTracker extends Entity
{
    public String getLabel();
    public void setLabel( String label);

    public String getKey();
    public void setKey( String key);

    public Integer getType();
    public void setType( Integer type);
}
