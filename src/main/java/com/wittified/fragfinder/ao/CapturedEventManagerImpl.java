package com.wittified.fragfinder.ao;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import net.java.ao.Query;

import javax.ws.rs.QueryParam;
import java.util.*;

/**
 * Created by daniel on 11/10/14.
 */
public class CapturedEventManagerImpl implements CapturedEventManager
{
    private final static int TRACKER_EVENT_TYPE = 0;
    private final static int TRACKER_HTTP_TYPE = 1;

    private final ActiveObjects activeObjects;
    private final TransactionTemplate transactionTemplate;

    public CapturedEventManagerImpl(
                                    final ActiveObjects activeObjects,
                                    final TransactionTemplate transactionTemplate
    )
    {
        this.activeObjects = activeObjects;
        this.transactionTemplate = transactionTemplate;
    }

    private void addTracker(final String name, final int type)
    {
        this.transactionTemplate.execute(new TransactionCallback<Object>()
        {
            @Override
            public Object doInTransaction()
            {
                NameTracker[] items = activeObjects.find( NameTracker.class, Query.select().where( " LABEL = ? AND type = ? ", name, type));

                if(items==null || items.length==0)
                {
                    NameTracker nameTracker = activeObjects.create( NameTracker.class);
                    nameTracker.setLabel( name);
                    nameTracker.setKey(name.toUpperCase().replaceAll("[^A-Z_0-9]", ""));
                    nameTracker.setType(type);
                    nameTracker.save();
                }
                return null;
            }
        });
    }

    public void registerEvent( final Date date, final String name, final  Map<String,String> params)
    {
        this.transactionTemplate.execute( new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {

                try {
                    CapturedEvent capturedEvent = activeObjects.create(CapturedEvent.class);
                    capturedEvent.setName(name);
                    capturedEvent.setTime(date);
                    capturedEvent.save();

                    for (String k : params.keySet()) {
                        CapturedEventParam param = activeObjects.create(CapturedEventParam.class);
                        param.setName(k);
                        param.setValue(params.get(k));
                        param.setCapturedEvent(capturedEvent);
                        param.save();
                    }
                }
                catch(Exception e){} // Ugly hack I know but the Active Objects are slow to load...

                return null;
            }
        });
        this.addTracker( name, TRACKER_EVENT_TYPE);
    }

    public List<TrackerContainer> getEventNames()
    {
        return this.transactionTemplate.execute( new TransactionCallback<List<TrackerContainer>>() {
            @Override
            public List<TrackerContainer> doInTransaction() {
                List<TrackerContainer> eventNames = new ArrayList<TrackerContainer>();


                for( NameTracker tracker: activeObjects.find(NameTracker.class, Query.select().where(" TYPE = ? ", TRACKER_EVENT_TYPE)) )
                {
                    TrackerContainer container = new TrackerContainer();
                    container.setNameTracker( tracker);

                    container.setNumItems(activeObjects.count( CapturedEvent.class, Query.select().where( " NAME = ? ", tracker.getLabel())) );
                    eventNames.add(container);
                }
                return eventNames;

            }
        });
    }

    public List<CapturedEvent> getEvents()
    {
        return this.transactionTemplate.execute( new TransactionCallback<List<CapturedEvent>>() {
            @Override
            public List<CapturedEvent> doInTransaction()
            {
                return Arrays.asList( activeObjects.find(CapturedEvent.class, Query.select().order( " TIME desc ")));
            }
        });
    }

    public PaginationEvents getEvents(final int start, final int end, final String filter, final String orderBy)
    {
        return this.transactionTemplate.execute( new TransactionCallback<PaginationEvents>() {
            @Override
            public PaginationEvents doInTransaction()
            {
                PaginationEvents paginationEvents = new PaginationEvents();
                if( start>0) {
                    paginationEvents.setPrevious(true);
                }
                Query query = Query.select();
                if( filter!=null && !filter.isEmpty())
                {
                    query = query.where(" NAME LIKE ? ", "%"+filter+"%");
                }
                String order = null;
                if( orderBy==null || orderBy.isEmpty())
                {
                    order = "TIME";
                }
                else
                {
                    order = orderBy.toUpperCase();
                }

                int numItems = end-start;


                CapturedEvent[] events = activeObjects.find( CapturedEvent.class, query.order(" "+ order+" desc ").offset(start).limit(numItems+1));
                if( events.length>numItems)
                {
                    paginationEvents.setNext(true);
                }

                if( events.length>numItems) {
                    paginationEvents.setEvents(Arrays.asList(events).subList(0, numItems));
                }
                else
                {
                    paginationEvents.setEvents(Arrays.asList( events));
                }

                return paginationEvents;
            }
        });
    }

    public void resetEvents()
    {
        this.transactionTemplate.execute( new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {

                activeObjects.delete( activeObjects.find( CapturedEventParam.class));
                activeObjects.delete( activeObjects.find( CapturedEvent.class));
                activeObjects.delete( activeObjects.find(NameTracker.class, Query.select().where(" TYPE = ?", TRACKER_EVENT_TYPE)));
                return null;
            }
        });
    }



    public void registerHTTP( final Date date, final String protocol, final String server, final String path, final String method, final String query,final  Map<String,String> headers, final String body)
    {
        this.transactionTemplate.execute( new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {
                try
                {

                    CapturedHTTP capturedHTTP = activeObjects.create( CapturedHTTP.class);
                    capturedHTTP.setDirection(1);
                    capturedHTTP.setMethod( method);
                    capturedHTTP.setQueryStr( query);
                    capturedHTTP.setTime( date);
                    capturedHTTP.setPath( path );
                    capturedHTTP.setProto( protocol);
                    capturedHTTP.setServer( server );
                    capturedHTTP.setBody( body );
                    capturedHTTP.save();

                    for(String name: headers.keySet())
                    {
                        CapturedHTTPHeader capturedHTTPHeader = activeObjects.create( CapturedHTTPHeader.class);
                        capturedHTTPHeader.setName( name);
                        capturedHTTPHeader.setValue( headers.get( name));
                        capturedHTTPHeader.setType(1);
                        capturedHTTPHeader.setCapturedHTTP(capturedHTTP);
                        capturedHTTPHeader.save();
                    }
                }
                catch(Exception e)
                {

                }
                return null;
            }
        });
        this.addTracker(server, TRACKER_HTTP_TYPE);

    }


    public PaginationHttp getHTTP(final int start, final int end,  final String order, final boolean inEnabled, final boolean outEnabled)
    {
        return this.transactionTemplate.execute(new TransactionCallback<PaginationHttp>() {
            @Override
            public PaginationHttp doInTransaction() {

                PaginationHttp paginationHttp = new PaginationHttp();
                if( start>0) {
                    paginationHttp.setPrevious(true);
                }


                String query = null;
                if( inEnabled && outEnabled)
                {
                    query = "";
                }
                else if( inEnabled && !outEnabled)
                {
                    query = " DIRECTION = 1 ";
                }

                else if( !inEnabled && outEnabled)
                {

                    query = " DIRECTION = 2 ";
                }
                else
                {
                    return paginationHttp;
                }

                int numItems = end-start;

                Query queryStmt = Query.select();
                if( !query.isEmpty())
                {
                    queryStmt = queryStmt.where(query);
                }

                CapturedHTTP[] https =  activeObjects.find(CapturedHTTP.class, queryStmt.order(" " + order + " desc ").offset(start).limit(numItems+1));
                if( https.length>numItems)
                {
                    paginationHttp.setNext(true);
                }

                if( https.length>numItems) {
                    paginationHttp.setHttps(Arrays.asList(https).subList(0, numItems));
                }
                else
                {
                    paginationHttp.setHttps(Arrays.asList( https));
                }

                return paginationHttp;
            }
        });

    }

    public PaginationHttp getHTTP( final int start,final  int end,final String host, final String order, final  boolean inEnabled, final boolean outEnabled)
    {
        return this.transactionTemplate.execute(new TransactionCallback<PaginationHttp>() {
            @Override
            public PaginationHttp doInTransaction() {

                PaginationHttp paginationHttp = new PaginationHttp();
                if( start>0) {
                    paginationHttp.setPrevious(true);
                }


                String query = null;
                if( inEnabled && outEnabled)
                {
                    query = "";
                }
                else if( inEnabled && !outEnabled)
                {
                    query = " AND DIRECTION = 1 ";
                }

                else if( !inEnabled && outEnabled)
                {

                    query = " AND DIRECTION = 2 ";
                }
                else
                {
                    return paginationHttp;
                }

                int numItems = end-start;




                CapturedHTTP[] https =  activeObjects.find(CapturedHTTP.class, Query.select( ).where( " SERVER = ? "+ query, host ).order(" "+ order+" desc ").offset(start).limit(numItems + 1));
                if( https.length>numItems)
                {
                    paginationHttp.setNext(true);
                }

                if( https.length>numItems) {
                    paginationHttp.setHttps(Arrays.asList(https).subList(0, numItems));
                }
                else
                {
                    paginationHttp.setHttps(Arrays.asList(https));
                }

                return paginationHttp;
            }
        });

    }

    public List<CapturedHTTP> getHTTP(final boolean inEnabled, final boolean outEnabled)
    {

        return this.transactionTemplate.execute(
                new TransactionCallback<List<CapturedHTTP>>() {
            @Override
            public List<CapturedHTTP> doInTransaction()
            {

                String query = null;
                if( inEnabled && outEnabled)
                {
                    return Arrays.asList( activeObjects.find(CapturedHTTP.class, Query.select().order( " TIME desc")));
                }
                else if( inEnabled && !outEnabled)
                {
                    query = " DIRECTION = 1 ";
                }

                else if( !inEnabled && outEnabled)
                {

                    query = " DIRECTION = 2 ";
                }
                else
                {
                    return new ArrayList<CapturedHTTP>();
                }


                return Arrays.asList( activeObjects.find(CapturedHTTP.class, Query.select().where(query).order( " TIME desc")));
            }
        });
    }


    public List<CapturedHTTP> getHTTP( final String host, final boolean inEnabled, final  boolean outEnabled)
    {
        return this.transactionTemplate.execute( new TransactionCallback<List<CapturedHTTP>>() {
            @Override
            public List<CapturedHTTP> doInTransaction()
            {

                String query = null;
                if( inEnabled && outEnabled)
                {
                    query = "";
                }
                else if( inEnabled && !outEnabled)
                {
                    query = " AND DIRECTION = 1 ";
                }

                else if( !inEnabled && outEnabled)
                {

                    query = " AND DIRECTION = 2 ";
                }
                else
                {
                    return new ArrayList<CapturedHTTP>();
                }

                return Arrays.asList( activeObjects.find(CapturedHTTP.class, Query.select( ).where( " SERVER = ? "+ query, host ).order( " TIME desc")));
            }
        });
    }

    public int countHttpHosts(final String filter,  final boolean outEnabled, final boolean inEnabled)
    {
        return this.transactionTemplate.execute( new TransactionCallback<Integer>() {
            @Override
            public Integer doInTransaction()
            {

                String query = null;
                if( inEnabled && outEnabled)
                {
                    query = "";
                }
                else if( inEnabled && !outEnabled)
                {
                    query = " DIRECTION = 1 ";
                }

                else if( !inEnabled && outEnabled)
                {

                    query = " DIRECTION = 2 ";
                }
                else
                {
                    return 0;
                }


                if( filter==null || filter.isEmpty())
                {
                    if( query==null || query.isEmpty())
                    {
                        return activeObjects.count(CapturedHTTP.class, Query.select( ));
                    }
                    return activeObjects.count(CapturedHTTP.class, Query.select( ).where(query));

                }
                if( query==null || query.isEmpty())
                {
                    return activeObjects.count(CapturedHTTP.class, Query.select().where(" SERVER = ? ", filter));
                }
                return activeObjects.count(CapturedHTTP.class, Query.select().where(" SERVER = ? AND " + query, filter));
            }
        });
    }

    public List<TrackerContainer> getHosts()
    {

        return this.transactionTemplate.execute( new TransactionCallback<List<TrackerContainer>>() {
            @Override
            public List<TrackerContainer> doInTransaction() {
                List<TrackerContainer> servers = new ArrayList<TrackerContainer>();


                for( NameTracker nameTracker: activeObjects.find(NameTracker.class, Query.select().where(" TYPE = ? ", TRACKER_HTTP_TYPE)) )
                {
                    TrackerContainer trackerContainer = new TrackerContainer();
                    trackerContainer.setNameTracker( nameTracker);
                    trackerContainer.setNumItems( activeObjects.count(CapturedHTTP.class, Query.select().where( " SERVER = ? ", nameTracker.getLabel())) );
                    servers.add( trackerContainer);
                }

                return servers;

            }
        });
    }

    public void resetHttp()
    {

        this.transactionTemplate.execute( new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {

                activeObjects.delete( activeObjects.find( CapturedHTTPHeader.class ));
                activeObjects.delete( activeObjects.find( CapturedHTTP.class));
                activeObjects.delete( activeObjects.find( NameTracker.class, Query.select().where(" TYPE = ?", TRACKER_HTTP_TYPE)));
                return null;
            }
        });
    }

    public void registerOutHTTP( final Date date, final String protocol, String serverName, final String path, final String method, final String query, final  Map<String,String> headers, final String body, final Map<String,String> requestHeaders, final String requestBody)
    {

        if( serverName.contains(":"))
        {
            if( (serverName.length()-serverName.replace(":","").length())==1)
            {
                serverName = serverName.substring(0, serverName.indexOf(":"));
            }
        }
        final String server = serverName;

        this.transactionTemplate.execute( new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {
                try
                {

                    final CapturedHTTP outCapturedHTTP = activeObjects.create( CapturedHTTP.class);
                    outCapturedHTTP.setDirection(2);
                    outCapturedHTTP.setMethod(method);
                    outCapturedHTTP.setQueryStr(query);
                    outCapturedHTTP.setTime(date);
                    outCapturedHTTP.setProto(protocol);
                    outCapturedHTTP.setServer(server);
                    outCapturedHTTP.setPath(path);
                    outCapturedHTTP.setBody( body);
                    outCapturedHTTP.setReqBody(requestBody);

                    outCapturedHTTP.save();

                    for(String name: headers.keySet())
                    {
                        CapturedHTTPHeader capturedHTTPHeader = activeObjects.create( CapturedHTTPHeader.class);
                        capturedHTTPHeader.setName( name);
                        capturedHTTPHeader.setType(0);
                        capturedHTTPHeader.setValue( headers.get( name));
                        capturedHTTPHeader.setCapturedHTTP(outCapturedHTTP);
                        capturedHTTPHeader.save();
                    }
                    for(String name: requestHeaders.keySet())
                    {
                        CapturedHTTPHeader capturedHTTPHeader = activeObjects.create( CapturedHTTPHeader.class);
                        capturedHTTPHeader.setName( name);
                        capturedHTTPHeader.setType(1);
                        capturedHTTPHeader.setValue( requestHeaders.get( name));
                        capturedHTTPHeader.setCapturedHTTP(outCapturedHTTP);
                        capturedHTTPHeader.save();
                    }
                }
                catch(Exception e)
                {

                }
                return null;
            }
        });
        this.addTracker(server, TRACKER_HTTP_TYPE);
    }

    public void expireContent()
    {

        try
        {
        this.transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction()
            {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.MINUTE, -10);
                Date cutOffDate = cal.getTime();

                for( CapturedEvent event: activeObjects.find( CapturedEvent.class, Query.select().where( " TIME < ? ", cutOffDate)))
                {
                    activeObjects.delete( event.getCapturedEventParams());
                    activeObjects.delete(event);
                }
                for( CapturedHTTP http: activeObjects.find( CapturedHTTP.class, Query.select().where( " TIME < ? ", cutOffDate)))
                {
                    activeObjects.delete( http.getCapturedHTTPHeaders());
                    activeObjects.delete( http);
                }
                return null;
            }
        });}
        catch(Exception e ){}
    }
}
