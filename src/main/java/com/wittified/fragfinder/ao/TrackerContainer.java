package com.wittified.fragfinder.ao;

/**
 * Created by daniel on 3/27/15.
 */
public class TrackerContainer
{
    private NameTracker nameTracker;
    private int numItems;

    public NameTracker getNameTracker() {
        return nameTracker;
    }

    public void setNameTracker(NameTracker nameTracker) {
        this.nameTracker = nameTracker;
    }

    public int getNumItems() {
        return numItems;
    }

    public void setNumItems(int numItems) {
        this.numItems = numItems;
    }
}
