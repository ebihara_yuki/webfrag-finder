package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DataStoreJob implements PluginJob
{
    private static Logger logger = LoggerFactory.getLogger(DataStoreJob.class);

    public void execute( Map<String, Object> jobData)
    {
        DataStoreService dataStoreService = (DataStoreService) jobData.get( "service");
        dataStoreService.store();
    }
}