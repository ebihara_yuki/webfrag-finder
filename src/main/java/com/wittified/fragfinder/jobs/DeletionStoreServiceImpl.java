package com.wittified.fragfinder.jobs;

import com.atlassian.plugin.StateAware;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.wittified.fragfinder.ao.CapturedEventManager;
import com.wittified.fragfinder.service.DataTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DeletionStoreServiceImpl implements DeletionStoreService,LifecycleAware, DisposableBean
{
    private final PluginScheduler pluginScheduler;
    private final Logger logger = LoggerFactory.getLogger(DeletionStoreServiceImpl.class);
    private final CapturedEventManager capturedEventManager;

    public DeletionStoreServiceImpl(final PluginScheduler pluginScheduler,
                                    final CapturedEventManager capturedEventManager)
    {
        this.pluginScheduler = pluginScheduler;
        this.capturedEventManager = capturedEventManager;
    }

    @Override
    public void onStart()
    {
        reschedule();
    }


    @Override
    public void reschedule()
    {
        Map<String, Object> scheduledData = new HashMap<String, Object>();

        scheduledData.put("service", this);

        this.pluginScheduler.scheduleJob(DeletionStoreServiceImpl.class.getName() + ":job", DeletionStoreJob.class,
                scheduledData, new Date(), 5*60*1000);

    }


    private boolean doNext = false;

    public void expire()
    {
        if(!doNext)

        {

            logger.debug("Transferring data");
            this.capturedEventManager.expireContent();
        }
        this.doNext = true;
    }

    @Override
    public void destroy() {

        this.pluginScheduler.unscheduleJob(DeletionStoreServiceImpl.class.getName());
    }
}
