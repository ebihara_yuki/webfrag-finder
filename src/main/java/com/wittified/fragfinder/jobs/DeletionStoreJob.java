package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DeletionStoreJob implements PluginJob
{
    private static Logger logger = LoggerFactory.getLogger(DeletionStoreJob.class);

    public void execute( Map<String, Object> jobData)
    {
        DeletionStoreService deletionStoreService = (DeletionStoreService) jobData.get( "service");
        deletionStoreService.expire();
    }
}