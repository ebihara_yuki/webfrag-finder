package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public class ACUpdateJob implements PluginJob
{
    private static Logger logger = LoggerFactory.getLogger(ACUpdateJob.class);

    public void execute( Map<String, Object> jobData)
    {
        ACUpdateService acUpdateService = (ACUpdateService) jobData.get( "service");
        acUpdateService.check();
    }
}

