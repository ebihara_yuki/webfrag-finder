package com.wittified.fragfinder.jobs;

import com.atlassian.plugin.StateAware;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.wittified.fragfinder.service.DataTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DataStoreServiceImpl implements DataStoreService,LifecycleAware, DisposableBean
{
    private final PluginScheduler pluginScheduler;
    private final Logger logger = LoggerFactory.getLogger(DataStoreServiceImpl.class);
    private final DataTracker dataTracker;

    public DataStoreServiceImpl( final PluginScheduler pluginScheduler,
                             final DataTracker dataTracker)
    {
        this.pluginScheduler = pluginScheduler;
        this.dataTracker = dataTracker;
    }

    @Override
    public void onStart()
    {
        reschedule();
    }

    @Override
    public void reschedule()
    {
        Map<String, Object> scheduledData = new HashMap<String, Object>();

        scheduledData.put("service", this);


        this.pluginScheduler.scheduleJob(DataStoreServiceImpl.class.getName() + ":job", DataStoreJob.class,
                scheduledData, new Date(), 10*1000);

    }

    public void store()
    {
        logger.debug("Transferring data");
        this.dataTracker.transferData();
    }


    @Override
    public void destroy()
    {

        this.pluginScheduler.unscheduleJob( DataStoreServiceImpl.class.getName()+":job");

    }
}
