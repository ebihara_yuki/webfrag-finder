package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daniel on 11/17/14.
 */
public class UsageReportJob implements PluginJob
{
    private static Logger logger = LoggerFactory.getLogger(UsageReportJob.class);

    public void execute( Map<String, Object> jobData)
    {
        UsageService usageService = (UsageService) jobData.get( "service");
        usageService.ping("active");
    }
}
