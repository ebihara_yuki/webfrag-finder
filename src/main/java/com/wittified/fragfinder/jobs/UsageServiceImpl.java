package com.wittified.fragfinder.jobs;

import com.atlassian.plugin.StateAware;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/17/14.
 */
public class UsageServiceImpl implements UsageService,LifecycleAware, DisposableBean
{
    private final PluginScheduler pluginScheduler;
    private final RequestFactory requestFactory;
    private final Logger logger = LoggerFactory.getLogger( UsageServiceImpl.class);

    public UsageServiceImpl( final PluginScheduler pluginScheduler,
                             final RequestFactory requestFactory)
    {
        this.pluginScheduler = pluginScheduler;
        this.requestFactory = requestFactory;
    }

    @Override
    public void onStart()
    {
        this.ping("start");
        reschedule();
    }

    @Override
    public void reschedule()
    {
        Map<String, Object> scheduledData = new HashMap<String, Object>();

        scheduledData.put("service", this);

        this.pluginScheduler.scheduleJob(UsageServiceImpl.class.getName() + ":job", UsageReportJob.class,
                scheduledData, new Date(), 60*60*1000);

    }

    public void ping( String eventType)
    {
        if( System.getProperty("webfrag.allow.google.tracking", "false").equalsIgnoreCase("false") && (System.getenv("webfragsBlockGoogle")==null))
        {

            AnalyticsConfigData analyticsConfigData = new AnalyticsConfigData("UA-38030585-6");

            JGoogleAnalyticsTracker jGoogleAnalyticsTracker = new JGoogleAnalyticsTracker(analyticsConfigData, JGoogleAnalyticsTracker.GoogleAnalyticsVersion.V_4_7_2);

            jGoogleAnalyticsTracker.trackEvent("webFragment", eventType);
            logger.info("Sending usage statistics to Google. This can be disabled by starting the tomcat instance with -Dwebfrag.allow.google.tracking=true");
        }



    }


    @Override
    public void destroy()
    {

        this.pluginScheduler.unscheduleJob( UsageServiceImpl.class.getName()+":job");

    }
}
