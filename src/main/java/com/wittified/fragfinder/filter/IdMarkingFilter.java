package com.wittified.fragfinder.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by daniel on 3/12/15.
 */
public class IdMarkingFilter implements Filter
{


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        if( servletRequest instanceof HttpServletRequest)
        {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;


            httpServletRequest.setAttribute("com.wittified.requestId", httpServletRequest.getRequestURI()+":"+System.currentTimeMillis() );
        }

        filterChain.doFilter( servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }
}
