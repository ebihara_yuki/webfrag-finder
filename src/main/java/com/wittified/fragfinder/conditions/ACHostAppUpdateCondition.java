package com.wittified.fragfinder.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.Constants;

import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public class ACHostAppUpdateCondition  implements Condition
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public ACHostAppUpdateCondition( final PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {

    }

    @Override
    public boolean shouldDisplay(Map<String, Object> params)
    {
        return (this.pluginSettingsFactory.createGlobalSettings().get("com.wittified.webfrags.ac.updateHost")!=null) || (this.pluginSettingsFactory.createGlobalSettings().get(Constants.SETTINGS_UPDATE_PLUGIN)!=null );
    }
}
