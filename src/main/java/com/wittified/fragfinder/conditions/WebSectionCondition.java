package com.wittified.fragfinder.conditions;

import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.servlets.PluginConfigurationServlet;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Copyright (c) 2013, Wittified, LLC
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


public class WebSectionCondition implements Condition
{
	private final PluginSettingsFactory pluginSettingsFactory;
    private final ApplicationProperties applicationProperties;

	private final String LOOK_FOR = "web.sections";

	public WebSectionCondition( final PluginSettingsFactory pluginSettingsFactory,
                                final ApplicationProperties applicationProperties)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.applicationProperties = applicationProperties;
    }

	@Override
	public void init(Map<String, String> stringStringMap) throws PluginParseException
	{
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public boolean shouldDisplay(Map<String, Object> stringObjectMap)
	{
        if( this.isStash()){ return false;}
		Object settingVal = this.pluginSettingsFactory.createGlobalSettings().get(PluginConfigurationServlet.ALWAYS_DISPLAY_WEB_SECTION);
		if ( (settingVal != null) && (Boolean.parseBoolean((String) settingVal)) ) {
			return true;
		}

		HttpServletRequest httpServletRequest;

		// jira does request
		if ( stringObjectMap.containsKey("request") ) {
			httpServletRequest = (HttpServletRequest) stringObjectMap.get("request");

			if ( (httpServletRequest != null) && (httpServletRequest.getParameterMap() != null) ) {
				return httpServletRequest.getParameterMap().containsKey(LOOK_FOR);
			}


		}

		// and Bamboo passes the query strings in...
		if ( stringObjectMap.containsKey(LOOK_FOR) ) {
			return true;
		}

		httpServletRequest = ServletContextThreadLocal.getRequest();
		if ( httpServletRequest != null ) {
			if ( (httpServletRequest != null) && (httpServletRequest.getParameterMap() != null) ) {
				return httpServletRequest.getParameterMap().containsKey(LOOK_FOR);
			}
		}


		return false;
	}

    private boolean isStash()
    {
        return this.applicationProperties.getDisplayName().equalsIgnoreCase("stash");
    }
}