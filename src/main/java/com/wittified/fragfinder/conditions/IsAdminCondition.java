package com.wittified.fragfinder.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.user.UserManager;

import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class IsAdminCondition implements Condition
{
    private final UserManager userManager;

    public IsAdminCondition( final UserManager userManager)
    {
        this.userManager = userManager;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException {

    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap)
    {
        if( this.userManager.getRemoteUsername()!=null)
        {
            return ( this.userManager.isAdmin( this.userManager.getRemoteUsername()));
        }
        return false;
    }
}
