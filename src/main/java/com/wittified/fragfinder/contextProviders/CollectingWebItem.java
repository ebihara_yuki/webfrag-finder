package com.wittified.fragfinder.contextProviders;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.model.DefaultWebLink;
import com.atlassian.sal.api.web.context.HttpContext;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.dom4j.Element;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by daniel on 2/6/15.
 */
public class CollectingWebItem implements ContextProvider
{
    private final HttpContext httpContext;
    public CollectingWebItem(
                                HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }


    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {

        context.put("webFragId", this.doCollection(this.httpContext.getRequest(), context));

        return context;
    }


    private String doCollection( HttpServletRequest req, Map<String, Object> context)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Map<String, Object> safeData = new HashMap<String, Object>();

        for(String k: context.keySet())
        {
            Object obj = context.get(k);
            if(obj!=null) {
                Map<String, String> map = new WeakHashMap<String, String>();
                map.put("class", obj.getClass().getCanonicalName());
                map.put("value", obj.toString());
                safeData.put(k, map);
            }

        }
        String data = null;
        try
        {
            data = objectMapper.writeValueAsString(safeData);
        }
        catch(IOException ioException)
        {
            ioException.printStackTrace();
        }

        HttpSession session = req.getSession(true);

        String entryName = (String) req.getAttribute("wittified.requestName");
        if(entryName==null)
        {
            entryName =  "webfrags-items:"+req.getRequestURI()+":"+System.currentTimeMillis();
            req.setAttribute("wittified.requestName", entryName);
        }

        Map<String, String> idsToData = (Map<String, String>) session.getAttribute(entryName);
        if(idsToData==null)
        {
            idsToData = new HashMap<String, String>();
        }


        String id = this.getName( data);
        idsToData.put( id, data);

        session.setAttribute(entryName, idsToData);

        session.setAttribute(entryName+":time", new Date());
        return id;
    }


    private String getName( String data)
    {
        try {
            byte[] mdBytes =  MessageDigest.getInstance("MD5").digest( data.getBytes());


            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < mdBytes.length; i++) {
                sb.append(Integer.toString((mdBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return "default-"+System.currentTimeMillis()+":"+Math.random();
    }
}
