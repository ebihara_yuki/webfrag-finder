package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.user.UserManager;
import com.wittified.fragfinder.service.ACEEditorService;
import com.wittified.fragfinder.service.DescriptorService;
import com.wittified.fragfinder.rest.entities.descriptor.PageEntity;
import com.wittified.fragfinder.rest.entities.descriptor.WebItemEntity;
import com.wittified.fragfinder.rest.entities.descriptor.WebPanelEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/1/14.
 */
@Path("/descriptor")
public class DescriptorEndPoint
{
    private final DescriptorService descriptorService;
    private final UserManager userManager;
    private final ACEEditorService aceEditorService;


    public DescriptorEndPoint( final DescriptorService descriptorService,
                               final UserManager userManager,
                               final ACEEditorService aceEditorService)
    {
        this.descriptorService = descriptorService;
        this.userManager = userManager;
        this.aceEditorService = aceEditorService;
    }

    @GET
    @Path("/keys")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllKeys()
    {
        return Response.ok(this.aceEditorService.getKeys() ).build();
    }

    @GET@Path("/get/{key}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getThis(@PathParam("key") String key)
    {
        Map<String, Object> retData = new HashMap<String, Object>();

        if( key.startsWith( this.aceEditorService.getCurrentPluginKey()))
        {
            key = this.aceEditorService.removePluginKey( key);
        }
        retData.put("key", key);


        Object module = this.descriptorService.getModule( key);

        if( module instanceof WebItemEntity)
        {
            retData.put("module", (WebItemEntity) module);
            retData.put("type", "webItem");
        }

        if( module instanceof PageEntity)
        {
            PageEntity pageEntity =  (PageEntity) module;
                    retData.put("module", pageEntity);
            retData.put("type", "page");


            if( this.descriptorService.isAdminPage(pageEntity))
            {
                retData.put("type", "adminPage");
            }
            if( this.descriptorService.isGeneralPage(pageEntity))
            {
                retData.put("type", "generalPage");
            }
        }

        return Response.ok(retData).build();

    }

    @Path("/panel")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response addPanel( WebPanelEntity webPanelEntity)
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.descriptorService.addWebPanel( webPanelEntity);
        this.aceEditorService.reload();
        return Response.ok( webPanelEntity).build();
    }


    @Path("/item")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response addItem( WebItemEntity webItemEntity)
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        if( webItemEntity.getTarget()!=null)
        {
            if(webItemEntity.getTarget().getOptions()!=null)
            {
                if(webItemEntity.getTarget().getOptions().containsKey("chrome"))
                {
                    try
                    {
                        webItemEntity.getTarget().getOptions().put( "chrome",new Boolean( (String) webItemEntity.getTarget().getOptions().get("chrome") ));
                    }
                    catch(Exception e){} // do nothing
                }
            }
        }
        this.descriptorService.addWebItem(webItemEntity);
        this.aceEditorService.reload();

        return Response.ok( webItemEntity).build();
    }


    @Path("/admin")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response addAdmin( PageEntity pageEntity)
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.descriptorService.addAdminPage(pageEntity);
        this.aceEditorService.reload();

        return Response.ok( pageEntity).build();
    }



    @Path("/general")
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response addGeneral( PageEntity pageEntity)
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.descriptorService.addGeneralPage(pageEntity);
        this.aceEditorService.reload();

        return Response.ok( pageEntity).build();
    }



}
