package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.web.context.HttpContext;
import com.google.gson.Gson;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by daniel on 2/6/15.
 */
@Path("/webfragdata")
public class WebFragContextResource
{
    private final HttpContext httpContext;

    public WebFragContextResource( final HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }

    @GET
    @Path("/data")
    @Produces({MediaType.APPLICATION_JSON})
    public Response data(@QueryParam("request") String requestName)
    {
        if( !requestName.startsWith("webfrags-items"))
        {
            return Response.status(403).build();
        }

        Map<String, String> data = (Map<String, String>) this.httpContext.getSession(true).getAttribute(requestName);

        if( data == null)
        {
            data = new HashMap<String, String>();
        }

        Gson gson = new Gson();
        Map<String, Map> values = new WeakHashMap<String, Map>();
        for( String k: data.keySet())
        {
            values.put(k, gson.fromJson(data.get(k),Map.class));
        }
        return Response.ok(values).build();
    }
}
