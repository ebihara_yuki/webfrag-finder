package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.rest.entities.ConfigEntity;
import com.wittified.fragfinder.service.ConfigurationService;
import com.wittified.fragfinder.service.HttpProxyService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by daniel on 3/6/15.
 */
public class ConfigurationEndPoints
{
    private final ConfigurationService configurationService;


    public ConfigurationEndPoints( final ConfigurationService configurationService)
    {
        this.configurationService = configurationService;
    }

    @GET
    @Path("/proxy")
    public Response getProxy()
    {
        ConfigEntity configEntity = new ConfigEntity();
        configEntity.setName( "proxy");
        configEntity.setState( this.configurationService.isHttpProxyEnabled() );

        return Response.ok( configEntity).build();

    }


    @POST
    @Path("/proxy")
    public Response setProxy( ConfigEntity configEntity)
    {

        if("proxy".equals( configEntity.getName() ) )
        {
            this.configurationService.setHttpProxyEnabled( configEntity.getState());
            if( configEntity.getState())
            {
             //   this.httpProxyService.start();
            }
            else
            {
               // this.httpProxyService.stop();
            }
        }



        return Response.ok( configEntity).build();

    }


}
