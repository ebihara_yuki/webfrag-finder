package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class LifeCycleEntity
{
    public String installed;
    public String uninstalled;

    public LifeCycleEntity() {}

    public String getInstalled() {
        return installed;
    }

    public void setInstalled(String installed) {
        this.installed = installed;
    }

    public String getUninstalled() {
        return uninstalled;
    }

    public void setUninstalled(String uninstalled) {
        this.uninstalled = uninstalled;
    }
}
