package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class ModuleEntities
{
    public List<PageEntity> generalPages;
    public List<PageEntity> adminPages;
    public PageEntity configurePage;
    public List<PageEntity> jiraComponentTabPanels;
    public List<PageEntity> profilePages;
    public List<WebItemEntity> webItems;
    public List<WebPanelEntity> webPanels;
    public List<PageEntity> jiraProjectAdminTabPanels;
    public List<PageEntity> jiraProjectTabPanels;
    public List<Object> reports;
    public List<PageEntity> jiraSearchRequestViews;
    public List<PageEntity> jiraVersionTabPanels;
    public List<Object> webhooks;
    public List<Object> jiraWorkflowPostFunctions;

    public ModuleEntities() {}
    public List<PageEntity> getGeneralPages() {
        return generalPages;
    }

    public void setGeneralPages(List<PageEntity> generalPages) {
        this.generalPages = generalPages;
    }

    public List<PageEntity> getAdminPages() {
        return adminPages;
    }

    public void setAdminPages(List<PageEntity> adminPages) {
        this.adminPages = adminPages;
    }

    public PageEntity getConfigurePage() {
        return configurePage;
    }

    public void setConfigurePage(PageEntity configurePage) {
        this.configurePage = configurePage;
    }

    public List<PageEntity> getJiraComponentTabPanels() {
        return jiraComponentTabPanels;
    }

    public void setJiraComponentTabPanels(List<PageEntity> jiraComponentTabPanels) {
        this.jiraComponentTabPanels = jiraComponentTabPanels;
    }

    public List<PageEntity> getProfilePages() {
        return profilePages;
    }

    public void setProfilePages(List<PageEntity> profilePages) {
        this.profilePages = profilePages;
    }

    public List<WebItemEntity> getWebItems() {
        return webItems;
    }

    public void setWebItems(List<WebItemEntity> webItems) {
        this.webItems = webItems;
    }

    public List<WebPanelEntity> getWebPanels() {
        return webPanels;
    }

    public void setWebPanels(List<WebPanelEntity> webPanels) {
        this.webPanels = webPanels;
    }

    public List<PageEntity> getJiraProjectAdminTabPanels() {
        return jiraProjectAdminTabPanels;
    }

    public void setJiraProjectAdminTabPanels(List<PageEntity> jiraProjectAdminTabPanels) {
        this.jiraProjectAdminTabPanels = jiraProjectAdminTabPanels;
    }

    public List<PageEntity> getJiraProjectTabPanels() {
        return jiraProjectTabPanels;
    }

    public void setJiraProjectTabPanels(List<PageEntity> jiraProjectTabPanels) {
        this.jiraProjectTabPanels = jiraProjectTabPanels;
    }

    public List<Object> getReports() {
        return reports;
    }

    public void setReports(List<Object> reports) {
        this.reports = reports;
    }

    public List<PageEntity> getJiraSearchRequestViews() {
        return jiraSearchRequestViews;
    }

    public void setJiraSearchRequestViews(List<PageEntity> jiraSearchRequestViews) {
        this.jiraSearchRequestViews = jiraSearchRequestViews;
    }

    public List<PageEntity> getJiraVersionTabPanels() {
        return jiraVersionTabPanels;
    }

    public void setJiraVersionTabPanels(List<PageEntity> jiraVersionTabPanels) {
        this.jiraVersionTabPanels = jiraVersionTabPanels;
    }

    public List<Object> getWebhooks() {
        return webhooks;
    }

    public void setWebhooks(List<Object> webhooks) {
        this.webhooks = webhooks;
    }

    public List<Object> getJiraWorkflowPostFunctions() {
        return jiraWorkflowPostFunctions;
    }

    public void setJiraWorkflowPostFunctions(List<Object> jiraWorkflowPostFunctions) {
        this.jiraWorkflowPostFunctions = jiraWorkflowPostFunctions;
    }
}
