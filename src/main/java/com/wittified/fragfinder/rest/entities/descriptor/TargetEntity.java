package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class TargetEntity
{
    public String type;
    public Map<String, Object> options;

    public TargetEntity() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getOptions() {
        return options;
    }

    public void setOptions(Map<String, Object> options) {
        this.options = options;
    }
}



