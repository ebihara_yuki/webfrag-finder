package com.wittified.fragfinder.rest.entities;

import com.wittified.fragfinder.ao.CapturedEvent;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/20/14.
 */
@JsonSerialize
public class PaginationEventsEntity
{
    @JsonSerialize
    public Boolean previous;
    @JsonSerialize
    public Boolean next;
    @JsonSerialize
    public List<CapturedEventEntity> events;

    public PaginationEventsEntity() {}

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public List<CapturedEventEntity> getEvents() {
        return events;
    }

    public void setEvents(List<CapturedEventEntity> events) {
        this.events = events;
    }
}
