package com.wittified.fragfinder.rest.entities.descriptor;

import com.wittified.fragfinder.rest.entities.descriptor.IconEntity;
import com.wittified.fragfinder.rest.entities.descriptor.NameEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class PageEntity
{
    public String url;
    public IconEntity icon;
    public String key;
    public NameEntity name;
    public String location;
    public List<Map<String, String>> conditions;
    public Integer weight;

    public PageEntity() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public IconEntity getIcon() {
        return icon;
    }

    public void setIcon(IconEntity icon) {
        this.icon = icon;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public NameEntity getName() {
        return name;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public List<Map<String, String>> getConditions() {
        return conditions;
    }

    public void setConditions(List<Map<String, String>> conditions) {
        this.conditions = conditions;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
