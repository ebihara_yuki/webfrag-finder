package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
@JsonSerialize
public class PaginationAOEntity
{
    @JsonSerialize
    public Boolean previous;
    @JsonSerialize
    public Boolean next;
    @JsonSerialize
    public List<AORowEntity> rows;

    @JsonSerialize
    public List<Map<String, String>> headers;

    public PaginationAOEntity() {}

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public List<AORowEntity> getRows() {
        return rows;
    }

    public void setRows(List<AORowEntity> events) {
        this.rows = events;
    }

    public List<Map<String, String>> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Map<String, String>> headers) {
        this.headers = headers;
    }
}
