package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/1/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class WebItemEntity
{
    public String location;
    public String url;
    public String label;
    public Integer weight;
    public String context;
    public String key;
    public List<String> styleClasses;
    public NameEntity name;
    public TargetEntity target;
    public Map<String, String> conditions;
    public IconEntity icon;



    public WebItemEntity() {}

    public List<String> getStyleClasses() {
        return styleClasses;
    }

    public void setStyleClasses(List<String> styleClasses) {
        this.styleClasses = styleClasses;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public NameEntity getName() {
        return name;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public TargetEntity getTarget() {
        return target;
    }

    public void setTarget(TargetEntity target) {
        this.target = target;
    }

    public Map<String, String> getConditions() {
        return conditions;
    }

    public void setConditions( Map<String, String> condition) {
        this.conditions = conditions;
    }

    public IconEntity getIcon() {
        return icon;
    }

    public void setIcon(IconEntity icon) {
        this.icon = icon;
    }
}
