package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/29/14.
 */
@JsonSerialize
public class HttpRequest
{
    @JsonSerialize
    private String responseCode;
    @JsonSerialize
    private String responseMessage;
    @JsonSerialize
    private String responseBody;
    @JsonSerialize
    private List<Map<String, String>> responseHeaders;
    @JsonSerialize
    private String requestPath;
    @JsonSerialize
    private String requestServer;
    @JsonSerialize
    private String requestMethod;
    @JsonSerialize
    private String requestBody;
    @JsonSerialize
    private List<Map<String, String>> requestHeaders;

    public HttpRequest() {}

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public List<Map<String, String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(List<Map<String, String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public String getRequestServer() {
        return requestServer;
    }

    public void setRequestServer(String requestServer) {
        this.requestServer = requestServer;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public List<Map<String, String>> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(List<Map<String, String>> requestHeaders) {
        this.requestHeaders = requestHeaders;
    }
}
