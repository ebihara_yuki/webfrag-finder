package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class AuthenticationEntity
{
    public String type;

    public AuthenticationEntity() {}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
