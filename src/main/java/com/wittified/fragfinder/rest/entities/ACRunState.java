package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 3/5/15.
 */
@JsonSerialize
public class ACRunState
{
    public String directory;
    public String command;
}
