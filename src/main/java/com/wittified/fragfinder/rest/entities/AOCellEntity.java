package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 11/24/14.
 */

@JsonSerialize
public class AOCellEntity
{
    @JsonSerialize
    private String name;
    @JsonSerialize
    private String value;

    public AOCellEntity() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
