package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/24/14.
 */
@JsonSerialize
public class AORowEntity
{
    @JsonSerialize
    List<AOCellEntity> entries;

    public AORowEntity() {}

    public List<AOCellEntity> getEntries() {
        return entries;
    }

    public void setEntries(List<AOCellEntity> entries) {
        this.entries = entries;
    }
}
