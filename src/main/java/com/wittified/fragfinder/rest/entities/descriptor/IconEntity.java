package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class IconEntity
{
    public String url;

    public Integer height;
    public Integer width;

    public IconEntity() {}

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }
}
