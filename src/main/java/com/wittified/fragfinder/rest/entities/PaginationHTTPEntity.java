package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/20/14.
 */
@JsonSerialize
public class PaginationHTTPEntity
{
    @JsonSerialize
    public Boolean previous;
    @JsonSerialize
    public Boolean next;
    @JsonSerialize
    public List<CapturedHttpEntity> httpRequests;

    public PaginationHTTPEntity() {}

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public List<CapturedHttpEntity> getHttpRequests() {
        return httpRequests;
    }

    public void setHttpRequests(List<CapturedHttpEntity> httpRequests) {
        this.httpRequests = httpRequests;
    }
}
