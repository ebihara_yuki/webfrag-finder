package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
@JsonSerialize
public class CapturedEventEntity
{
    @JsonSerialize
    private String name;
    @JsonSerialize
    private List<Map<String, String>> params;
    @JsonSerialize
    private Date time;

    public CapturedEventEntity() {}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Map<String, String>> getParams() {
        return params;
    }

    public void setParams(List<Map<String, String>> params) {
        this.params = params;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
