package com.wittified.fragfinder.rest.entities;


import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 3/6/15.
 */
@JsonSerialize
public class ConfigEntity
{
    public Boolean state;
    public String name;

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
