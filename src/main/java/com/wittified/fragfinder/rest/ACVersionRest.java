package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.jobs.ACUpdateService;
import com.wittified.fragfinder.service.VersionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by daniel on 12/5/14.
 */
@Path("/acversion")
public class ACVersionRest
{
    private final PluginSettingsFactory pluginSettingsFactory;
    private final VersionService versionService;
    private final ACUpdateService acUpdateService;

    public ACVersionRest( final PluginSettingsFactory pluginSettingsFactory,
                          final VersionService versionService,
                          final ACUpdateService acUpdateService)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.versionService = versionService;
        this.acUpdateService = acUpdateService;
    }

    @Path("/update")
    @Produces({MediaType.APPLICATION_JSON})
    @POST
    public Response updatePlugins()
    {
        Map<String, String> state = new WeakHashMap<String, String>();
        String env = (String) this.pluginSettingsFactory.createGlobalSettings().get("com.wittified.webfrags.acversion");
        if( env!=null)
        {
            state.put("env", env);
            state.put("success", Boolean.toString( this.versionService.updatePlugins(env)));
            this.acUpdateService.check();

        }
        return Response.ok(state).build();
    }

    @Path("/update/{target}")
    @Produces({MediaType.APPLICATION_JSON})
    @POST
    public Response updatePlugins(@PathParam("target") String env)
    {
        Map<String, String> state = new WeakHashMap<String, String>();
        if( env!=null)
        {
            state.put("env", env);
            state.put("success", Boolean.toString( this.versionService.updatePlugins(env)));

            this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.acversion", env);
            this.acUpdateService.check();

        }
        return Response.ok(state).build();
    }

    @Path("/status")
    @Produces({MediaType.APPLICATION_JSON})
    @GET
    public Response checkStatus()
    {
        return Response.ok( this.versionService.isPluginUpgradeInProgress()).build();
    }


    @Path("/leave")
    @Produces({MediaType.APPLICATION_JSON})
    @GET
    public Response leaveACMode()
    {
        System.clearProperty("atlassian.upm.on.demand");
        return Response.ok().build();
    }
}
