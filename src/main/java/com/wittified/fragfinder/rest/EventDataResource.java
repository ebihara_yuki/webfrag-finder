package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.user.UserManager;
import com.wittified.fragfinder.ao.*;
import com.wittified.fragfinder.rest.entities.CapturedEventEntity;
import com.wittified.fragfinder.rest.entities.NavEntryEntity;
import com.wittified.fragfinder.rest.entities.PaginationEventsEntity;
import com.wittified.fragfinder.service.DataTracker;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
@Path("/webfrags/events")
public class EventDataResource
{
    private final UserManager userManager;
    private final CapturedEventManager capturedEventManager;
    private final DataTracker dataTracker;


    public EventDataResource( final CapturedEventManager capturedEventManager,
                              final UserManager userManager,
                              final DataTracker dataTracker)
    {
        this.capturedEventManager = capturedEventManager;
        this.userManager = userManager;
        this.dataTracker = dataTracker;
    }

    @POST
    @Path("/refresh")
    @Produces({MediaType.APPLICATION_JSON})
    public Response refresh()
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.dataTracker.transferData();
        return Response.ok().build();

    }

    @GET
    @Path("/page/{pageNum}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPaginatedEvents( @PathParam("pageNum") Integer pageNum, @QueryParam("sort") String sortBy, @QueryParam("numItems") Integer numItems, @QueryParam("filter") String filter )
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }
        pageNum--;
        if( numItems==null)
        {
            numItems = 20;
        }
        if( filter == null)
        {
            filter = "";
        }

        PaginationEventsEntity paginationEventsEntity = new PaginationEventsEntity();

        PaginationEvents paginationEvents = this.capturedEventManager.getEvents(pageNum*numItems,(pageNum+1)* numItems,filter, sortBy);
        paginationEventsEntity.setNext( paginationEvents.getNext());
        paginationEventsEntity.setPrevious( paginationEvents.getPrevious());


        List<CapturedEventEntity> capturedEventEntities = new ArrayList<CapturedEventEntity>();
        for( CapturedEvent capturedEvent: paginationEvents.getEvents())
        {
            capturedEventEntities.add( this.convert( capturedEvent));
        }
        paginationEventsEntity.setEvents( capturedEventEntities);

        return Response.ok( paginationEventsEntity ).build();

    }

    @GET
    @Path("/eventNames")
    @Produces( {MediaType.APPLICATION_JSON})
    public Response getEventNames()
    {


        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        List<NavEntryEntity> navEntryEntities = new ArrayList<NavEntryEntity>();
        for( TrackerContainer nav: this.capturedEventManager.getEventNames())
        {
            NavEntryEntity navEntryEntity = new NavEntryEntity();
            navEntryEntity.setKey( nav.getNameTracker().getKey());
            navEntryEntity.setName( nav.getNameTracker().getLabel());
            navEntryEntity.setNumItems( nav.getNumItems());
            navEntryEntities.add( navEntryEntity);
        }
        return Response.ok( navEntryEntities).build();
    }

    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllEvents()
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }


        List<CapturedEventEntity> capturedEventEntities = new ArrayList<CapturedEventEntity>();
        for( CapturedEvent capturedEvent: this.capturedEventManager.getEvents())
        {
            capturedEventEntities.add( this.convert( capturedEvent));
        }

        return Response.ok( capturedEventEntities).build();

    }

    @GET
    @Path("/reset")
    @Produces({MediaType.APPLICATION_JSON})
    public Response reset()
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }
        this.capturedEventManager.resetEvents();
        return Response.ok().build();
    }

    protected CapturedEventEntity convert(CapturedEvent capturedEvent)
    {
        CapturedEventEntity capturedEventEntity = new CapturedEventEntity();
        capturedEventEntity.setName( capturedEvent.getName());
        capturedEventEntity.setTime( capturedEvent.getTime());

        List<Map<String, String>> params = new ArrayList<Map<String, String>>();
        for(CapturedEventParam p: capturedEvent.getCapturedEventParams())
        {
            Map<String, String> entry = new HashMap<String, String>();
            entry.put("name", p.getName());
            entry.put("value", p.getValue());

            params.add( entry);
        }

        capturedEventEntity.setParams( params);

        return capturedEventEntity;

    }
}
