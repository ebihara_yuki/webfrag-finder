package com.wittified.fragfinder.events;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.ao.CapturedEventManager;
import com.wittified.fragfinder.service.ACEEditorService;
import com.wittified.fragfinder.service.DataTracker;
import com.wittified.fragfinder.service.DescriptorService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
public class EventCapturingListener  implements InitializingBean, DisposableBean
{
    private final EventPublisher eventPublisher;
    private final DataTracker dataTracker;
    private final ACEEditorService aceEditorService;


    public EventCapturingListener( final EventPublisher eventPublisher,
                                   final DataTracker dataTracker,
                                   final ACEEditorService aceEditorService)
    {
        this.eventPublisher = eventPublisher;
        this.dataTracker = dataTracker;
        this.aceEditorService = aceEditorService;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        this.eventPublisher.register( this );
    }

    @Override
    public void destroy() throws Exception
    {

        this.eventPublisher.unregister( this );
    }

    @EventListener
    public void onAddonUpload( PluginEnabledEvent pluginEnabledEvent)
    {
        String key =  pluginEnabledEvent.getPlugin().getKey();
        String watchForKey = this.aceEditorService.getCurrentPluginKey();
        if( key.equals(watchForKey))
        {
            this.aceEditorService.reload();
        }

    }

    @EventListener
    public void onAllEvents( Object eventObject)
    {
        String className = eventObject.getClass().getName();
        Map<String, String> data = new HashMap<String, String>();

        for( Field field: eventObject.getClass().getDeclaredFields())
        {
            try {
                if( field.getModifiers() == Modifier.PUBLIC) {
                    data.put(field.getName(), field.get(eventObject).toString());
                }
            }catch(IllegalAccessException e)
            {
                // do nothing
            }
        }

        for(Method method: eventObject.getClass().getMethods())
        {
            if( method.getName().startsWith("get"))
            {
                try {
                    if( method.getModifiers() == Modifier.PUBLIC) {
                        data.put(method.getName(), method.invoke(eventObject).toString());
                    }
                }catch(Exception e)
                {
                    // do nothing
                }
            }
        }
        this.dataTracker.storeEventData( className, data);
    }

}
