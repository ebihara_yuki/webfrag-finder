package com.wittified.fragfinder;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.*;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.wittified.fragfinder.events.EventCapturingListener;
import com.wittified.fragfinder.service.ConfigurationService;
import com.wittified.fragfinder.service.DataTracker;
import com.wittified.fragfinder.service.HttpProxyService;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import org.apache.commons.io.IOUtils;
import org.littleshoot.proxy.*;
import org.littleshoot.proxy.extras.SelfSignedMitmManager;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.impl.ProxyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Copyright (c) 2013, Wittified, LLC
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


public class PluginEnablingListener implements LifecycleAware, DisposableBean
{
	private static Logger logger = LoggerFactory.getLogger(PluginEnablingListener.class);

	private final LocationService locationService;
	private final PluginController pluginController;
	private final DefaultPluginArtifactFactory pluginArtifactFactory;
	private final TemplateRenderer templateRenderer;
    private final ConfigurationService configurationService;
    private final HttpProxyService httpProxyService;
    private final PluginScheduler pluginScheduler;





    public PluginEnablingListener(final LocationService locationService,
								  final PluginController pluginController,
								  final TemplateRenderer templateRenderer,
                                  final ConfigurationService configurationService,
                                  final HttpProxyService httpProxyService,
                                    final PluginScheduler pluginScheduler)

	{
		this.locationService = locationService;
		this.pluginController = pluginController;
		this.templateRenderer = templateRenderer;
        this.httpProxyService = httpProxyService;
        this.configurationService = configurationService;
        this.pluginScheduler = pluginScheduler;

		this.pluginArtifactFactory = new DefaultPluginArtifactFactory();
	}

    public void enabled()
    {
        logger.debug("Plugin has started");


    }


	public void onStart()
	{

        logger.debug("Plugin has started");



        logger.debug("Uploading");
        this.uploadOurPlugin();

        logger.debug("Upload completed");

            this.httpProxyService.start();

        //now add our logger
//        LogCapturerAppender logCapturerAppender = new LogCapturerAppender( this.logEntryManager);

    //    org.apache.log4j.Logger.getLogger( "org.apache.http.wire").removeAllAppenders();
  //      org.apache.log4j.Logger.getLogger( "org.apache.http.wire").addAppender(logCapturerAppender);
//        org.apache.log4j.Logger.getLogger( "org.apache.http.wire").setLevel(Level.DEBUG);


//        logger.debug("Tied into logging");

	}

    @Override
    public void destroy() throws Exception {
        this.httpProxyService.stop();
    }


    public void pluginEnabled(PluginEnabledEvent pluginEnabledEvent)
	{
		logger.info(String.format("Plugin was enabled: %s", pluginEnabledEvent.getPlugin().getKey()));
		if( !pluginEnabledEvent.getPlugin().getKey().equals("autogenerated.wittified.web") )
		{
			this.uploadOurPlugin();
		}
		else
		{
			logger.info("Skipping triggering on our plugin -- infinite loop and all that");
		}
	}

    private void uploadOurPlugin()
    {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("pluginArtifactFactory", this.pluginArtifactFactory);
        data.put("pluginController", this.pluginController);
        data.put("templateRenderer", this.templateRenderer);
        data.put("locationService", this.locationService);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 3);
        logger.info("Triggering the plugin uploading in 3 seconds from now");
        this.pluginScheduler.scheduleJob(GenerateAndUploadPluginJob.class.getName()+":job", GenerateAndUploadPluginJob.class, data, calendar.getTime(), 3600000);

    }

}
