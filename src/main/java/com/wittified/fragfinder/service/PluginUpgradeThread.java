package com.wittified.fragfinder.service;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.Constants;
import com.wittified.fragfinder.jobs.ACUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 3/29/15.
 */
public class PluginUpgradeThread implements Runnable
{
    private final String MAVEN_BASE = "https://maven.atlassian.com/repository/public/";

    private final static Logger logger = LoggerFactory.getLogger( PluginUpgradeThread.class);

    private LinkedHashMap<String, Map<String, String>> pluginData;

    private  boolean hasStarted = false;
    private  boolean completed = false;

    private PluginController pluginController;
    private RequestFactory requestFactory;


    public PluginController getPluginController() {
        return pluginController;
    }

    public void setPluginController(PluginController pluginController) {
        this.pluginController = pluginController;
    }

    public RequestFactory getRequestFactory() {
        return requestFactory;
    }

    public void setRequestFactory(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public LinkedHashMap<String, Map<String, String>> getPluginData() {
        return pluginData;
    }

    public void setPluginData(LinkedHashMap<String, Map<String, String>> pluginData) {
        this.pluginData = pluginData;
    }

    @Override
    public void run() {

        this.hasStarted = true;
        this.completed = false;

        List<File> filesToUpdate = new ArrayList<File>();
        System.setProperty("atlassian.upm.on.demand","true");


        boolean usingTempFiles = false;
        File pluginDir = new File(new File( new File(  System.getProperty("user.home") ),".webfrags"),"plugins") ;
        if( !pluginDir.exists()) {
            if( !pluginDir.mkdirs() )
            {
                logger.error("Unable to create "+ pluginDir.getAbsolutePath()+" Using temporary path");
                try {
                    pluginDir = File.createTempFile("webfrags", "plugin");
                    pluginDir.mkdirs();
                    usingTempFiles = true;
                }catch(IOException e)
                {
                    e.printStackTrace();
                    logger.error("Unable to create temporary file. Unable to install things.");
                    this.completed = true;
                    return;
                }
            }
        }

        logger.info("Plugin directory is at "+ pluginDir.getAbsolutePath());

        for( String pluginKey : pluginData.keySet())
        {
            Map<String, String> thisPluginData = pluginData.get( pluginKey);
            try {




                URL theUrl =  fetchAndStoreArtifact( pluginDir, thisPluginData.get("package"),
                        thisPluginData.get("artifact"),
                        thisPluginData.get("version")
                        );

                File tempFile = File.createTempFile("webfrag", "."+thisPluginData.get("extension"));

                logger.info("Downloading " + theUrl.toExternalForm());
                org.apache.commons.io.FileUtils.copyURLToFile(theUrl, tempFile);

                filesToUpdate.add(tempFile);
            }
            catch(IOException io    )
            {
                io.printStackTrace();
            }
        }
        System.setProperty("atlassian.upm.on.demand","true");


        for( File artifactFile: filesToUpdate )
        {
            logger.debug("Installing addon");

            logger.debug("Done: "+ this.pluginController.installPlugins( new JarPluginArtifact( artifactFile)) );
        }




        for( File artifactFile: filesToUpdate )
        {
            artifactFile.delete();

        }

        this.completed = true;

        if(usingTempFiles)
        {
            pluginDir.delete();
        }
    }




    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isHasStarted() {
        return hasStarted;
    }

    public void setHasStarted(boolean hasStarted) {
        this.hasStarted = hasStarted;
    }




    private URL fetchAndStoreArtifact( File pluginDir, String groupId, String packageId, String versionId) throws IOException
    {

        File artifactFile = null;
        if(pluginDir!=null)
        {
            artifactFile = new File( pluginDir, groupId+"."+packageId+"-"+versionId+".obr");
            if( !artifactFile.exists())
            {
                artifactFile = new File( pluginDir, groupId+"."+packageId+"-"+versionId+".jar");
            }
        }
        if ((artifactFile!=null ) && (artifactFile.exists()))
        {
            return artifactFile.toURI().toURL();
        }


        String url = null;
        String jarUrl = MAVEN_BASE+(groupId.replaceAll("\\.","/"))+"/"+packageId+"/"+versionId+"/"+packageId+"-"+versionId+".jar";
        String obrUrl = MAVEN_BASE+(groupId.replaceAll("\\.","/"))+"/"+packageId+"/"+versionId+"/"+packageId+"-"+versionId+".obr";


        try {
            this.requestFactory.createRequest(Request.MethodType.HEAD, jarUrl).execute();
            url = jarUrl;

        }
        catch(ResponseException e)
        {
            e.printStackTrace();
        }
        if(url==null)
        {
            try {
                this.requestFactory.createRequest(Request.MethodType.HEAD, obrUrl).execute();
                url = obrUrl;
            }
            catch(ResponseException e)
            {
                e.printStackTrace();
            }

        }
        if(url == null)
        {
            return null;
        }

        // download the item and cache it.


        logger.info(String.format("Downloading %s to %s", url, artifactFile.getAbsoluteFile()));
        org.apache.commons.io.FileUtils.copyURLToFile( new URL(url), artifactFile);

        return artifactFile.toURI().toURL();
    }

}
