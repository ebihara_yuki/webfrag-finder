package com.wittified.fragfinder.service;

import com.wittified.fragfinder.rest.entities.descriptor.DescriptorEntity;
import com.wittified.fragfinder.rest.entities.descriptor.PageEntity;
import com.wittified.fragfinder.rest.entities.descriptor.WebItemEntity;
import com.wittified.fragfinder.rest.entities.descriptor.WebPanelEntity;

import java.util.Set;

/**
 * Created by daniel on 11/3/14.
 */
public interface DescriptorService
{
    public boolean doesDescriptorExist();
    public boolean createDescriptor( String path );
    public void saveDescriptorPath( String path );

    public DescriptorEntity getDescriptor();

    public Object getModule(String key);
    public Set<String> getAllKeys();

    public boolean isAdminPage (PageEntity entity);
    public boolean isGeneralPage (PageEntity entity);


    public boolean addWebItem( WebItemEntity webItemEntity);
    public boolean addAdminPage( PageEntity adminPageEntity);
    public boolean addGeneralPage( PageEntity generalPage);
    public boolean addWebPanel( WebPanelEntity webPanelEntity);
}
