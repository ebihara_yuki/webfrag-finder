package com.wittified.fragfinder.service;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by daniel on 3/5/15.
 */
public class NodeACERunnerImpl implements ACERunner, DisposableBean
{
    Process nodeProcess;

    private final static Logger logger = LoggerFactory.getLogger( NodeACERunnerImpl.class);


    private File lastBaseDir = null;
    private String lastStartCommand = null;
    public NodeACERunnerImpl(final PluginAccessor pluginAccessor)
    {
        Plugin  plugin = pluginAccessor.getPlugin("com.wittified.webfragment-finder");
        this.nodeProcess = null;

    }

    @Override
    public void start()
    {
        if(( this.lastBaseDir!=null) && ( this.lastStartCommand!=null))
        {
            this.start(this.lastBaseDir, this.lastStartCommand);
        }
    }


    @Override
    public void start(File baseDir, String startCommand)
    {
        if(this.nodeProcess != null)
        {
            logger.error("Node is already running");
            return;
        }

        this.lastBaseDir = baseDir;
        this.lastStartCommand = startCommand;

        File appFile = new File( baseDir, startCommand);



        System.err.println("Starting node at "+ baseDir.getAbsolutePath());
        System.err.println("Starting node with "+ appFile.getName());
        logger.info("Running node");
        ProcessBuilder processBuilder = new ProcessBuilder( "node", appFile.getName());
        try {
            Map<String, String> env = processBuilder.environment();
            env.putAll( System.getenv());
            env.put("env", "development");
            env.put("AC_OPTS", "force-reg");
            this.nodeProcess =processBuilder.inheritIO().directory(baseDir).start();


        }
        catch(IOException ioException)
        {
            ioException.printStackTrace();
        }
        logger.info("Node has been started");
    }

    @Override
    public void stop()
    {
        logger.info("Stopping node");
        if(this.nodeProcess==null)
        {
            return;
        }
        logger.debug("not null");
        try
        {
            this.nodeProcess.exitValue();
            logger.info("Node is not running anymore");
            this.nodeProcess = null;
            return;
        }
        catch(IllegalThreadStateException e)
        {

        }
        logger.info("Destroying node");
        this.nodeProcess.destroy();
        this.nodeProcess = null;

        logger.info("Node destroyed");
    }

    @Override
    public boolean isCurrentlyRunning()
    {
        if( this.nodeProcess==null)
        {
            return false;
        }
        try
        {
            this.nodeProcess.exitValue();
        }
        catch(IllegalThreadStateException e)
        {
            return true;
        }
        return false;
    }

    @Override
    public void destroy() throws Exception
    {

        if( this.isCurrentlyRunning())
        {
            this.stop();
        }
    }
}
