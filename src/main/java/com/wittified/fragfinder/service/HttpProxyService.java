package com.wittified.fragfinder.service;

/**
 * Created by daniel on 3/5/15.
 */
public interface HttpProxyService
{
    public boolean start();
    public void stop();
}
