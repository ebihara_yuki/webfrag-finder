package com.wittified.fragfinder.service;

import java.io.File;

/**
 * Created by daniel on 3/5/15.
 */
public interface ACERunner
{
    public void start(File baseDir, String startCommand);
    public void start();
    public void stop();
    public boolean isCurrentlyRunning();
}
