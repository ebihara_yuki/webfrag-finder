package com.wittified.fragfinder.service;

import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public interface VersionService
{
    public boolean isHostedAppUpToDate( String env );
    public boolean arePluginsUpToDate( String env );

    public String getCurrentPublishedVersion( String env );

    public Map<String, Map<String, String>> getPluginsNeedingUpdate( String env );

    public boolean updatePlugins( String env );

    public boolean isPluginUpgradeInProgress();


}
