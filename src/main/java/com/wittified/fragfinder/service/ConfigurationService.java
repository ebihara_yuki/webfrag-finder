package com.wittified.fragfinder.service;

/**
 * Created by daniel on 3/6/15.
 */
public interface ConfigurationService
{
    public boolean isHttpProxyEnabled();
    public void setHttpProxyEnabled( boolean enabled);
}
