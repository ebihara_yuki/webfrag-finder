package com.wittified.fragfinder.service;

import com.wittified.fragfinder.ao.CapturedEventManager;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.*;

/**
 * Created by daniel on 11/20/14.
 */
public class DataTrackerImpl implements DataTracker
{

    public List<Map<String, Object>> eventData;
    public List<Map<String, Object>> httpData;
    public List<Map<String, Object>> outHttpData;

    private final CapturedEventManager capturedEventManager;

    public DataTrackerImpl( final CapturedEventManager capturedEventManager)
    {
        this.eventData = new ArrayList<Map<String, Object>>();
        this.httpData = new ArrayList<Map<String, Object>>();
        this.outHttpData = new ArrayList<Map<String, Object>>();
        this.capturedEventManager = capturedEventManager;

    }

    public void storeEventData( String name, Map<String,String> params)
    {
        Map<String, Object> entry = new HashMap<String, Object>();
        entry.put("date", new Date());
        entry.put("name", name);
        entry.put("params", params);
        this.eventData.add( entry);
    }

    public void storeHTTPData( String protocol, String server, String path, String method, Map<String, String> headers, String query, String body)
    {
        ObjectMapper objectMapper = new ObjectMapper();


        Map<String, Object> entry = new HashMap<String, Object>();
        entry.put("date", new Date());
        entry.put("path", path);
        entry.put("server", server);
        entry.put("protocol", protocol);

        entry.put("method", method);
        entry.put("headers",headers);
        entry.put("query", query);
        entry.put("body", body);
        this.httpData.add( entry);
    }


    public void storeOutHTTPData( String protocol, String server, String path,String method, Map<String, String> headers, String query, String content, Map<String, String> requestHeaders, String requestBody)
    {

        Map<String, Object> entry = new HashMap<String, Object>();
        entry.put("date", new Date());
        entry.put("path", path);
        entry.put("server", server);
        entry.put("protocol", protocol);
        entry.put("method", method);
        entry.put("headers",headers);
        entry.put("query", query);
        entry.put("content", content);
        entry.put("requestBody", requestBody);
        entry.put("requestHeaders", requestHeaders);
        this.outHttpData.add( entry);
    }

    public void transferData()
    {
        try {
            for (Map<String, Object> data : Collections.synchronizedList(this.eventData)) {
                this.capturedEventManager.registerEvent((Date) data.get("date"), (String) data.get("name"), (Map<String, String>) data.get("params"));
            }
        }
        catch(Exception e) {}  // Catch any exception
        this.eventData = new ArrayList<Map<String, Object>>();


        try {

            for (Map<String, Object> data : Collections.synchronizedList(this.httpData)) {
                this.capturedEventManager.registerHTTP((Date) data.get("date"), (String) data.get("protocol"),(String) data.get("server"),(String) data.get("path"), (String) data.get("method"), (String) data.get("query"), (Map<String, String>) data.get("headers"), (String) data.get("body"));
            }
        }
        catch(Exception e) {}  // Catch any exception
        this.httpData = new ArrayList<Map<String, Object>>();
        try {

            for (Map<String, Object> data : Collections.synchronizedList(this.outHttpData)) {
                this.capturedEventManager.registerOutHTTP((Date) data.get("date"), (String) data.get("protocol"),(String) data.get("server"),(String) data.get("path"), (String) data.get("method"), (String) data.get("query"), (Map<String, String>) data.get("headers"), (String) data.get("content"),(Map<String, String>) data.get("requestHeaders"), (String) data.get("requestBody"));
            }
        }
        catch(Exception e) {}  // Catch any exception
        this.outHttpData = new ArrayList<Map<String, Object>>();

    }
}
