package com.wittified.fragfinder.service;

import java.util.List;

/**
 * Created by daniel on 3/29/15.
 */
public interface ACEEditorService
{
    public List<String> getKeys();
    public String getCurrentPluginKey();
    public String removePluginKey( String key);
    public void reload();
}
