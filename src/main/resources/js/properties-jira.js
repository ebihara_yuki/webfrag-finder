if(!window.wittified)
{
window.wittified = {};
}
if( !window.wittified.webfrags)
{
    window.wittified.webfrags = {};
}
if( !window.wittified.webfrags.jira)
{
    window.wittified.webfrags.jira = {};
}


window.wittified.webfrags.jira.properties =
{
    'mode': 'issue',
    'item': false,
    'subitem': false,
    'load': function()
    {
        AJS.$('#wittified-nav').html( com.wittified.fragfinder.client.jira.properties.navArea({}) );

        AJS.$('.wittified-plugin-property-link').on('click', function(evt)
        {
            evt.preventDefault();
            AJS.$('#properties-'+wittified.webfrags.jira.properties.mode).removeClass('aui-nav-selected');
            wittified.webfrags.jira.properties.mode = AJS.$(this).data('type');
            AJS.$('#properties-'+wittified.webfrags.jira.properties.mode).addClass('aui-nav-selected');
            wittified.webfrags.jira.properties.renderInitialize();
        });
        this.renderInitialize();

    },
    'renderInitialize': function()
    {
        AJS.$('#properties-content').html( '' );

        var initSelectButton = function()
        {
            AJS.$('#select-button').on('click', function(evt)
            {
                evt.preventDefault();
                wittified.webfrags.jira.properties.item = AJS.$('#selector-value').val();
                if(AJS.$('#selector-subvalue').length)
                {
                    wittified.webfrags.jira.properties.subitem = AJS.$('#selector-subvalue').val();
                }
                wittified.webfrags.jira.properties.renderContent();
            });
        }

        if( this.mode=='issue')
        {
            AJS.$('#properties-selector').html( com.wittified.fragfinder.client.jira.properties.issueSelector({}) );
            initSelectButton();
        }
        else if( this.mode=='project')
        {
            AJS.$('#properties-selector').html( com.wittified.fragfinder.client.jira.properties.projectSelector({}) );
            initSelectButton();
        }
        else if( this.mode=='dashboard')
        {
            AJS.$.get( AJS.contextPath()+'/rest/api/2/dashboard?maxResults=1000', function(dashboards)
            {
                AJS.$('#properties-selector').html( com.wittified.fragfinder.client.jira.properties.dashboardSelector({dashboards:dashboards.dashboards}) );
                initSelectButton();
            })
        }

    },
    'renderContent': function()
    {
        if( wittified.webfrags.jira.properties.mode=='issue')
        {
            AJS.$.get( AJS.contextPath()+'/rest/api/2/issue/'+wittified.webfrags.jira.properties.item+'/properties', function(data)
            {
                AJS.$('#properties-content').html(com.wittified.fragfinder.client.jira.properties.renderContent({'data':data.keys}));
                AJS.$('#add-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showAddProperty();
                });

                AJS.$('.edit-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showEditProperty( AJS.$(this).data('id'));
                });


                AJS.$('.show-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showContent( AJS.$(this).data('id'));
                });
                AJS.$('.remove-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.deleteContent( AJS.$(this).data('id'));
                });

            }).fail(function() {
                  alert( "An error occurred looking up the issue key. Please try again." );
                })
        }
        else if( wittified.webfrags.jira.properties.mode=='project')
        {
            AJS.$.get( AJS.contextPath()+'/rest/api/2/project/'+wittified.webfrags.jira.properties.item+'/properties', function(data)
            {
                AJS.$('#properties-content').html(com.wittified.fragfinder.client.jira.properties.renderContent({'data':data.keys}));
                AJS.$('#add-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showAddProperty();
                });

                AJS.$('.edit-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showEditProperty( AJS.$(this).data('id'));
                });


                AJS.$('.show-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.showContent( AJS.$(this).data('id'));
                });
                AJS.$('.remove-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.jira.properties.deleteContent( AJS.$(this).data('id'));
                });

            }).fail(function() {
                  alert( "An error occurred looking up the project key. Please try again." );
                })
        }
        else if( wittified.webfrags.jira.properties.mode=='dashboard')
        {
            AJS.$.get( AJS.contextPath()+'/rest/api/2/dashboard/'+wittified.webfrags.jira.properties.item+'/items/'+ wittified.webfrags.jira.properties.subitem+'/properties', function(data)
            {
             AJS.$('#properties-content').html(com.wittified.fragfinder.client.jira.properties.renderContent({'data':data.keys}));
             AJS.$('#add-property').on('click', function(evt)
             {
                 evt.preventDefault();
                 wittified.webfrags.jira.properties.showAddProperty();
             });

             AJS.$('.edit-property').on('click', function(evt)
             {
                 evt.preventDefault();
                 wittified.webfrags.jira.properties.showEditProperty( AJS.$(this).data('id'));
             });


             AJS.$('.show-property').on('click', function(evt)
             {
                 evt.preventDefault();
                 wittified.webfrags.jira.properties.showContent( AJS.$(this).data('id'));
             });
             AJS.$('.remove-property').on('click', function(evt)
             {
                 evt.preventDefault();
                 wittified.webfrags.jira.properties.deleteContent( AJS.$(this).data('id'));
             });

            }).fail(function() {
               alert( "An error occurred looking up the dashboard widget. Please try again." );
             })
        }
    },
    'showAddPropertyDialog': false,
    'showAddProperty': function()
    {
        if(!this.showAddPropertyDialog)
        {
            this.showAddPropertyDialog =new AJS.Dialog({
                                            width: 360,
                                            height: 425,
                                            id: "add-properties-dialog",
                                            closeOnOutsideClick: true
                                        });
            this.showAddPropertyDialog.addHeader('Add property');
            this.showAddPropertyDialog.addPanel('Panel 1', '<div id="wittified-add-property-content">&nbsp;</div>');
            this.showAddPropertyDialog.addButtonPanel();
            this.showAddPropertyDialog.addLink('Cancel', function(dialog)
            {
                dialog.hide();
            });
            this.showAddPropertyDialog.addSubmit('Add', function(dialog)
            {
                var keyVal = AJS.$('#new-property-key').val();
                var valVal = AJS.$('#new-property-value').val();

                if(!keyVal)
                {
                    alert('Key is required');
                    return;
                }

                if( wittified.webfrags.jira.properties.mode=='issue')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/issue/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                         'type': 'PUT',
                         'data': valVal,
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.jira.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }

                else if( wittified.webfrags.jira.properties.mode=='project')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/project/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                         'type': 'PUT',
                         'data': valVal,
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.jira.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }

                else if( wittified.webfrags.jira.properties.mode=='dashboard')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/dashboard/'+wittified.webfrags.jira.properties.item+'/items/'+ wittified.webfrags.jira.properties.subitem+'/properties/'+keyVal,
                         'type': 'PUT',
                         'data': valVal,
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.jira.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }
                 //

            });
        }
        AJS.$('#wittified-add-property-content').html(com.wittified.fragfinder.client.jira.properties.renderNewProperty({}));
        this.showAddPropertyDialog.show();
    },
    'showEditPropertyDialog': false,
    'showEditProperty': function(keyVal)
    {
        if(!this.showEditPropertyDialog)
        {
            this.showEditPropertyDialog =new AJS.Dialog({
                                            width: 360,
                                            height: 375,
                                            id: "add-properties-dialog",
                                            closeOnOutsideClick: true
                                        });
            this.showEditPropertyDialog.addHeader('Edit property');
            this.showEditPropertyDialog.addPanel('Panel 1', '<div id="wittified-edit-property-content">&nbsp;</div>');
            this.showEditPropertyDialog.addButtonPanel();
            this.showEditPropertyDialog.addLink('Cancel', function(dialog)
            {
                dialog.hide();
            });
            this.showEditPropertyDialog.addSubmit('Save', function(dialog)
            {
                var valVal = AJS.$('#edit-property-value').val();
                if( wittified.webfrags.jira.properties.mode=='issue')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/issue/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                         'type': 'PUT',
                         'data': valVal,
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.jira.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }
                 else if( wittified.webfrags.jira.properties.mode=='project')
                  {

                      AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/project/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                           'type': 'PUT',
                           'data': valVal,
                           'contentType':'application/json',
                           'success':function()
                           {
                               wittified.webfrags.jira.properties.renderContent();
                               dialog.hide();
                           }
                       });
                   }


            });
        }
        AJS.$('#wittified-edit-property-content').html('&nbsp;');
        if( wittified.webfrags.jira.properties.mode=='issue')
               {
                   AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/issue/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                        'type': 'GET',
                        'contentType':'application/json',
                        'success':function(data)
                        {
                           AJS.$('#wittified-edit-property-content').html(com.wittified.fragfinder.client.jira.properties.renderEditProperty({"value":JSON.stringify(data.value)}));
                       }
                   });
               }
                else if( wittified.webfrags.jira.properties.mode=='project')
                       {
                           AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/project/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                                'type': 'GET',
                                'contentType':'application/json',
                                'success':function(data)
                                {
                                   AJS.$('#wittified-edit-property-content').html(com.wittified.fragfinder.client.jira.properties.renderEditProperty({"value":JSON.stringify(data.value)}));
                               }
                           });
                       }
        else if (wittified.webfrags.jira.properties.mode=='dashboard')
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/dashboard/'+wittified.webfrags.jira.properties.item+'/items/'+ wittified.webfrags.jira.properties.subitem+'/properties/'+keyVal,
                 'type': 'GET',
                 'contentType':'application/json',
                 'success':function(data)
                 {
                    AJS.$('#wittified-edit-property-content').html(com.wittified.fragfinder.client.jira.properties.renderEditProperty({"value":JSON.stringify(data.value)}));
                }
            });


        }
        this.showEditPropertyDialog.show();
    },
    'showContent': function(keyVal)
    {
        if( wittified.webfrags.jira.properties.mode=='issue')
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/issue/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                     'type': 'GET',
                     'contentType':'application/json',
                     'success':function(data)
                     {
                        AJS.$('#wittified-content-'+keyVal).html( JSON.stringify(data.value));
                    }
                });
        }
        else if( wittified.webfrags.jira.properties.mode=='project')
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/project/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                     'type': 'GET',
                     'contentType':'application/json',
                     'success':function(data)
                     {
                        AJS.$('#wittified-content-'+keyVal).html( JSON.stringify(data.value));
                    }
                });
        }
        else if( wittified.webfrags.jira.properties.mode=='dashboard')
         {
             AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/dashboard/'+wittified.webfrags.jira.properties.item+'/items/'+ wittified.webfrags.jira.properties.subitem+'/properties/'+keyVal,
                      'type': 'GET',
                      'contentType':'application/json',
                      'success':function(data)
                      {
                         AJS.$('#wittified-content-'+keyVal).html( JSON.stringify(data.value));
                     }
                 });
         }
    },

    'deleteContent': function(keyVal)
    {
        if( wittified.webfrags.jira.properties.mode=='issue')
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/issue/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                 'type': 'DELETE',
                 'contentType':'application/json',
                 'success':function(data)
                 {
                         wittified.webfrags.jira.properties.renderContent();
                }
            });
        }
        else if( wittified.webfrags.jira.properties.mode=='project')
         {
             AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/project/'+ wittified.webfrags.jira.properties.item+'/properties/'+keyVal,
                  'type': 'DELETE',
                  'contentType':'application/json',
                  'success':function(data)
                  {
                          wittified.webfrags.jira.properties.renderContent();
                 }
             });
         }
        else  if( wittified.webfrags.jira.properties.mode=='dashboard')
                     {
                         AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/2/dashboard/'+wittified.webfrags.jira.properties.item+'/items/'+ wittified.webfrags.jira.properties.subitem+'/properties/'+keyVal,
                              'type': 'DELETE',
                              'contentType':'application/json',
                              'success':function(data)
                              {
                                      wittified.webfrags.jira.properties.renderContent();
                             }
                         });
                     }

    }





}



AJS.toInit( function()
{
    if(AJS.$('#properties-selector').length)
    {
        wittified.webfrags.jira.properties.load();
    }
});