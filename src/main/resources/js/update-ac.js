AJS.toInit( function()
{

    var newVersion = AJS.$('meta[name="wittified-updateApp-new"]').attr('content');
    var currentVersion = AJS.$('meta[name="wittified-updateApp-current"]').attr('content');
    var shouldUpdatePlugins = AJS.$('meta[name="wittified-updatePlugins"]').attr('content');
    var upgradeInProgress = AJS.$('meta[name="wittified-appUpgradeInProgress"]').attr('content');


    window.WebFragsPollForUpdate = function()
    {
        AJS.$.get(AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/upgrades/status', function(data)
        {
            if( data.in_progress)
            {
                if( data.progress==100)
                {

                    var d = new AJS.Dialog({
                                width: 300,
                                height: 200,
                                id: "webfrags-dialog",
                                closeOnOutsideClick: false
                            });
                            d.addHeader('Upgrade ready');
                            d.addPanel('panel','<div id="upgrade-contents">The upgrade to '+newVersion+' is ready to be installed. Do you want to proceed? Once this is done you will have to start the application again.</div>');
                            d.addButtonPanel();
                            d.addButton('Cancel', function(d)
                            {
                                d.hide();
                            });
                            d.addButton('Upgrade', function()
                            {
                                AJS.$('#upgrade-contents').html('Please restart the application now.');
                                AJS.$.post(AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/upgrades/activate', function(d)
                                {
                                    AJS.$('#upgrade-contents').html('In Progress. Page will reload once completed');
                                    window.setInterval( function()
                                    {
                                        AJS.$.get(AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/upgrades/status', function(data)
                                        {
                                            if(data && data.completed)
                                            {
                                                AJS.$('#upgrade-contents').html('Upgrade done');
                                                window.location = window.location;
                                            }
                                        });
                                    }, 10000);
                                });

                            });
                    d.show();
                }
                else
                {
                    AJS.$('#wittified-update-app').attr('disabled', 'disabled');
                    AJS.$('#wittified-update-app').html('Downloading '+data.progress+'%');
                    window.setTimeout("window.WebFragsPollForUpdate();", 5000);
                }
            }
        });


    };

    if( (currentVersion && newVersion && currentVersion!=newVersion) || (shouldUpdatePlugins && shouldUpdatePlugins=='true'))
    {
       AJS.$('body').first().prepend('<div id="wittified-message-bar"></div>');
   }
   if( currentVersion && newVersion && currentVersion!=newVersion)
   {
        AJS.messages.info(AJS.$("#wittified-message-bar"),{
            title: "Cloud version has updated!",
//            body: "This instance is currently running "+ currentVersion+". The cloud instances are running "+newVersion+'. <button class="aui-button aui-button-primary" id="wittified-update-app">Update now</button>'
            body: "This instance is currently running "+ currentVersion+". The cloud instances are running "+newVersion+'.'
        });

        AJS.$('#wittified-update-app').click(function(event)
        {
            event.preventDefault();
            AJS.$('#wittified-update-app').attr('disabled', 'disabled');
            AJS.$('#wittified-update-app').html('Downloading 0%');

            AJS.$.post(AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/upgrades/'+newVersion, function(data)
            {
                if( data!=1)
                {
                    AJS.$('#wittified-update-app').html('Failed');
                    alert('An issue occured while upgrading. Please review the logs');
                    return;
                }


//                window.WebFragsPollForUpdate();
            });
        });


    }

    if( upgradeInProgress)
    {
//        window.WebFragsPollForUpdate();
    }

    if(shouldUpdatePlugins && shouldUpdatePlugins=='true')
    {

        AJS.messages.info(AJS.$("#wittified-message-bar"),{
            title: "Atlassian Connect addon has updated!",
            body: 'Ready to update? <button class="aui-button aui-button-primary" id="wittified-update-ac">Update now</button>'

        });

        AJS.$('#wittified-update-ac').click(function(event)
        {
            event.preventDefault();
            AJS.$('#wittified-update-ac').attr('disabled', 'disabled');
            AJS.$('#wittified-update-ac').html('In Progress');



            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acversion/update',
                'type': 'POST',
                'contentType': 'application/json',
                'success': function()
            {

                window.wittifiedPluginUpdatePoller = window.setInterval( function()
                {
                    AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acversion/status', function(s)
                    {
                        if(!s)
                        {

                            AJS.$('#wittified-update-ac').html('Upgrade completed');
                            window.clearInterval( window.wittifiedPluginUpdatePoller);
                        }
                    })
                }, 5000);

            }});
        });

    }

});