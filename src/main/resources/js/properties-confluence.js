if(!window.wittified)
{
window.wittified = {};
}
if( !window.wittified.webfrags)
{
    window.wittified.webfrags = {};
}
if( !window.wittified.webfrags.confluence)
{
    window.wittified.webfrags.confluence = {};
}


window.wittified.webfrags.confluence.properties =
{
    'mode': 'content',
    'item': false,
    'subitem': false,
    'load': function()
    {
        AJS.$('#wittified-nav').html( com.wittified.fragfinder.client.confluence.properties.navArea({}) );

        AJS.$('.wittified-plugin-property-link').on('click', function(evt)
        {
            evt.preventDefault();
            AJS.$('#properties-'+wittified.webfrags.confluence.properties.mode).removeClass('aui-nav-selected');
            wittified.webfrags.confluence.properties.mode = AJS.$(this).data('type');
            AJS.$('#properties-'+wittified.webfrags.confluence.properties.mode).addClass('aui-nav-selected');
            wittified.webfrags.confluence.properties.renderInitialize();
        });
        this.renderInitialize();

    },
    'renderInitialize': function()
    {
        AJS.$('#properties-content').html( '' );

        var initSelectButton = function()
        {
            AJS.$('#select-button').on('click', function(evt)
            {
                evt.preventDefault();
                wittified.webfrags.confluence.properties.item = AJS.$('#selector-value').val();
                if(AJS.$('#selector-subvalue').length)
                {
                    wittified.webfrags.confluence.properties.subitem = AJS.$('#selector-subvalue').val();
                }
                console.log( 'rendering' + wittified.webfrags.confluence.properties.item);
                wittified.webfrags.confluence.properties.renderContent();
            });
        }

        if( this.mode=='content')
        {
            AJS.$('#properties-selector').html( com.wittified.fragfinder.client.confluence.properties.contentSelector({}) );
            initSelectButton();
        }

    },
    'renderContent': function()
    {
        if( wittified.webfrags.confluence.properties.mode=='content')
        {
            AJS.$.get( AJS.contextPath()+'/rest/api/content/'+wittified.webfrags.confluence.properties.item+'/property?limit=1000&expand=key,value', function(data)
            {
                AJS.$('#properties-content').html(com.wittified.fragfinder.client.confluence.properties.renderContent({'data':data.results}));
                AJS.$('#add-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.confluence.properties.showAddProperty();
                });

                AJS.$('.edit-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.confluence.properties.showEditProperty( AJS.$(this).data('id'));
                });
                AJS.$('.remove-property').on('click', function(evt)
                {
                    evt.preventDefault();
                    wittified.webfrags.confluence.properties.deleteContent( AJS.$(this).data('id'));
                });

            }).fail(function() {
                  alert( "An error occurred looking up the content id. Please try again." );
                })
        }
    },
    'showAddPropertyDialog': false,
    'showAddProperty': function()
    {
        if(!this.showAddPropertyDialog)
        {
            this.showAddPropertyDialog =new AJS.Dialog({
                                            width: 360,
                                            height: 425,
                                            id: "add-properties-dialog",
                                            closeOnOutsideClick: true
                                        });
            this.showAddPropertyDialog.addHeader('Add property');
            this.showAddPropertyDialog.addPanel('Panel 1', '<div id="wittified-add-property-content">&nbsp;</div>');
            this.showAddPropertyDialog.addButtonPanel();
            this.showAddPropertyDialog.addLink('Cancel', function(dialog)
            {
                dialog.hide();
            });
            this.showAddPropertyDialog.addSubmit('Add', function(dialog)
            {
                var keyVal = AJS.$('#new-property-key').val();
                var valVal = AJS.$('#new-property-value').val();

                if(!keyVal)
                {
                    alert('Key is required');
                    return;
                }

                if( wittified.webfrags.confluence.properties.mode=='content')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/content/'+ wittified.webfrags.confluence.properties.item+'/property',
                         'type': 'POST',
                         'data': {"key":keyVal,"value": JSON.parse( valVal)},
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.jira.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }

            });
        }
        AJS.$('#wittified-add-property-content').html(com.wittified.fragfinder.client.confluence.properties.renderNewProperty({}));
        this.showAddPropertyDialog.show();
    },
    'showEditPropertyDialog': false,
    'showEditProperty': function(keyVal)
    {
        if(!this.showEditPropertyDialog)
        {
            this.showEditPropertyDialog =new AJS.Dialog({
                                            width: 360,
                                            height: 375,
                                            id: "add-properties-dialog",
                                            closeOnOutsideClick: true
                                        });
            this.showEditPropertyDialog.addHeader('Edit property');
            this.showEditPropertyDialog.addPanel('Panel 1', '<div id="wittified-edit-property-content">&nbsp;</div>');
            this.showEditPropertyDialog.addButtonPanel();
            this.showEditPropertyDialog.addLink('Cancel', function(dialog)
            {
                dialog.hide();
            });
            this.showEditPropertyDialog.addSubmit('Save', function(dialog)
            {
                var valVal = AJS.$('#edit-property-value').val();
                if( wittified.webfrags.confluence.properties.mode=='content')
                {

                    AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/content/'+ wittified.webfrags.confluence.properties.item+'/property/'+keyVal,
                         'type': 'PUT',
                         'json': JSON.stringify({'key':keyVal, 'value':JSON.parse(valVal),'version':{'number':new Date().getTime(), 'minorEdit':true}}),
                         'contentType':'application/json',
                         'success':function()
                         {
                             wittified.webfrags.confluence.properties.renderContent();
                             dialog.hide();
                         }
                     });
                 }


            });
        }
        AJS.$('#wittified-edit-property-content').html('&nbsp;');
        if( wittified.webfrags.confluence.properties.mode=='content')
               {
                   AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/content/'+ wittified.webfrags.confluence.properties.item+'/property/'+keyVal,
                        'type': 'GET',
                        'contentType':'application/json',
                        'success':function(data)
                        {
                           AJS.$('#wittified-edit-property-content').html(com.wittified.fragfinder.client.confluence.properties.renderEditProperty({"value":JSON.stringify(data.value)}));
                       }
                   });
               }        this.showEditPropertyDialog.show();
    },

    'deleteContent': function(keyVal)
    {
        if( wittified.webfrags.confluence.properties.mode=='content')
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/api/content/'+ wittified.webfrags.confluence.properties.item+'/property/'+keyVal,
                 'type': 'DELETE',
                 'contentType':'application/json',
                 'success':function(data)
                 {
                         wittified.webfrags.confluence.properties.renderContent();
                }
            });
        }
    }





}



AJS.toInit( function()
{
    if(AJS.$('#properties-selector').length)
    {
        wittified.webfrags.confluence.properties.load();
    }
});