if(!window.wittified)
{
window.wittified = {};
}
if( !window.wittified.webfrags)
{
    window.wittified.webfrags = {};
}


if( !window.wittified.webfrags.ace)
{

    window.wittified.webfrags.ace =
    {
        'start':
        {
            'dialog': false,
            'show': function(path)
            {
                if(!this.dialog)
                {
                    this.dialog = new AJS.Dialog({
                       width: 400,
                       height: 200,
                       id: 'ace-start-dialog',
                       closeOnOutsideClick: true
                    });
                    this.dialog.addHeader('Start?');
                    this.dialog.addPanel('Start-dialog', '<div id="wittified-start-ace">'+com.wittified.fragfinder.client.renderStartACEQuestion({})+'</div>');
                    this.dialog.addButtonPanel();
                    this.dialog.addButton('No', function(dialog)
                    {
                        dialog.hide();
                    });
                    this.dialog.addButton('Yes', function(dialog)
                    {
                        dialog.gotoPage(1);
                        AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acrun/start',
                             'type': 'POST',
                             'contentType': 'application/json',
                             'data': JSON.stringify({'directory': path, 'command':'app.js'}),
                             'success':function()
                             {
                                wittified.webfrags.ace.start.dialog.gotoPage(2);
                             }
                         });

                    });
                    this.dialog.addPage();

                    this.dialog.addHeader('Starting');
                    this.dialog.addPanel('Start-dialog', '<div id="wittified-starting-ace">'+com.wittified.fragfinder.client.renderStartACEProgress({})+'</div>');

                    this.dialog.addPage();

                    this.dialog.addHeader('Completed');
                    this.dialog.addPanel('Start-dialog', '<div id="wittified-completed-ace">'+com.wittified.fragfinder.client.renderStartACEResult({})+'</div>');
                    this.dialog.addButtonPanel();
                    this.dialog.addButton('Close', function(dialog)
                    {
                        window.location= window.location;
                    });

                }
                this.dialog.gotoPage(0);
                this.dialog.show();
            }
        },

        'stop':
        {
            'dialog': false,
            'show': function(path)
            {
                if(!this.dialog)
                {
                    this.dialog = new AJS.Dialog({
                       width: 400,
                       height: 200,
                       id: 'ace-start-dialog',
                       closeOnOutsideClick: true
                    });
                    this.dialog.addHeader('Stop?');
                    this.dialog.addPanel('Stop-dialog', '<div id="wittified-stop-ace">'+com.wittified.fragfinder.client.renderStopACEQuestion({})+'</div>');
                    this.dialog.addButtonPanel();
                    this.dialog.addButton('No', function(dialog)
                    {
                        dialog.hide();
                    });
                    this.dialog.addButton('Yes', function(dialog)
                    {
                        AJS.$('#wittified-stop-ace').html(com.wittified.fragfinder.client.renderStopACEProgress({}));

                        AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acrun/stop',
                             'type': 'POST',
                             'contentType': 'application/json',
                             'data':'{}',
                             'success':function()
                             {
                                AJS.$('#wittified-stop-ace').html(com.wittified.fragfinder.client.renderStopACEResult({}));
                                wittified.webfrags.ace.stop.dialog.hide();
                             }
                         });

                    });

                }
                this.dialog.show();
            }
        }
    }
}
AJS.toInit( function()
{
    AJS.$('.wittified-ace-running-stop').on('click', function(event)
    {
        event.preventDefault();
        wittified.webfrags.ace.stop.show();
    });

    AJS.$('.wittified-install-ac').on('click', function(event)
    {
        event.preventDefault();
        var btn = AJS.$(this);
        btn.attr('disabled', 'disabled');
        btn.html('In Progress');



        AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acversion/update/prd',
            'type': 'POST',
            'contentType': 'application/json',
            'success': function()
        {

            window.wittifiedPluginUpdatePoller = window.setInterval( function()
            {
                AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acversion/status', function(s)
                {
                    if(!s)
                    {

                        btn.html('Upgrade completed');
                        window.clearInterval( window.wittifiedPluginUpdatePoller);
                    }
                })
            }, 5000);

        }});

    });



    AJS.$('.wittified-leave-ac').on('click', function(event)
    {
        event.preventDefault();
        var btn = AJS.$(this);
        btn.attr('disabled', 'disabled');
        btn.html('In Progress');


        AJS.$.get(AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/acversion/leave', function(data)
        {
            btn.html('Removal completed');

        });
    });
});
