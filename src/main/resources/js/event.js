if(!window.wittified)
{
window.wittified = {};
}
if( !window.wittified.webfrags)
{
    window.wittified.webfrags = {};
}


window.wittified.webfrags.utils =
{
    'renderPagination': function( currPage, totalNumPages, target)
    {
        currPage--;
        var thisTarget =  AJS.$('#paginationarea');
        var maxPageNum = 10;
        var minPageNum = 0;
        if( currPage>5)
        {
            minPageNum = currPage-5;
        }
        if( currPage>5)
        {
            maxPageNum = currPage+5;
        }
        if( totalNumPages<maxPageNum)
        {
            maxPageNum = totalNumPages;
        }
        console.log('rendering pagination for '+totalNumPages);

        thisTarget.html( com.wittified.fragfinder.client.renderPagination({
            'currPage': currPage,
            'maxPage':maxPageNum,
            'minPage':minPageNum,
            'target': target,
            'hasMorePages': (totalNumPages-maxPageNum)>0
        }));

        thisTarget.find('.wittified-pagination-pageNumber').on('click', function(event)
        {
            event.preventDefault();
            var btn = AJS.$(this);
            var target = btn.data('target');
            var pageNum = btn.data('id');
            if( target=='events')
            {
                wittified.webfrags.events.renderPage( pageNum+1);
            }
            else if( target=='http')
            {
                wittified.webfrags.http.renderPage( pageNum+1);
            }

        });

    }
};

if( !window.wittified.webfrags.activeObjects)
{

    window.wittified.webfrags.activeObjects =
    {
        'dialog': false,
        'show': function( evt)
        {

            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/activeobjects/plugins', function( plugins)
            {

                for(p=0;p<plugins.length;p++)
                {
                    plugins[p].escapedKey=plugins[p].key.split('.').join('-');
                }
                AJS.$('#wittified-nav').html(com.wittified.fragfinder.client.renderAOPlugins({ plugins: plugins}));

                AJS.$('.wittified-plugin-ao-link').on('click', function(idx)
                {
                    var key = AJS.$(this).data('key');

                    window.wittified.webfrags.activeObjects.renderThisPlugin( key);
                });
//                window.wittified.webfrags.activeObjects.renderThisPlugin( plugins[0].key);
            });

        },
        'currentPlugin': false,
        'renderThisPlugin': function( key)
        {

            if( this.currentPlugin)
            {
                var escapedKey = this.currentPlugin.split('.').join('-');
                AJS.$('#ao-'+escapedKey).removeClass('aui-nav-selected');
            }
            this.currentPlugin = key;
            var escapedKey = this.currentPlugin.split('.').join('-');

            AJS.$('#ao-'+escapedKey).addClass('aui-nav-selected');
            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/activeobjects/tables/'+key, function( tables)
            {
                var tableDisplay = [];
                var hasTable = {};
                for(var t=0;t<tables.length;t++)
                {
                    var searchForName = true;
                    var brokenUp = tables[t].split('.');

                    var newName = '';
                    for( var b=brokenUp.length-1;b>-1;b--)
                    {
                        if( searchForName )
                        {
                            if(newName) { newName = newName+'.';}
                            newName =newName+ brokenUp[b];
                            if( !hasTable[newName])
                            {
                                searchForName = false;
                                hasTable[newName] = 1;

                                var escapedTableKey = tables[t].split('.').join('-');
                                tableDisplay.push({ 'key': tables[t],'escapedKey':escapedTableKey, 'name': newName});
                            }
                        }
                    }

                }

                AJS.$('#wittified-ao-plugin-contents').html( com.wittified.fragfinder.client.renderAOTabSelection({ tables: tableDisplay,escapedKey:escapedKey}) );
                window.wittified.webfrags.activeObjects.renderThisTable( tableDisplay[0].key, 1);
                AJS.$('.wittified-table-selecter').on('click', function(idx)
                {
                    window.wittified.webfrags.activeObjects.renderThisTable( AJS.$(this).data('tablekey'),1);
                });
            });
        },
        'currentTable': false,
        'currentTablePageNum': 0,
        'renderTableData': function()
        {
            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/activeobjects/data/'+window.wittified.webfrags.activeObjects.currentPlugin+'/'+ window.wittified.webfrags.activeObjects.currentTable+'/'+window.wittified.webfrags.activeObjects.currentTablePageNum, function (data)
            {
                var escapedKey = window.wittified.webfrags.activeObjects.currentTable.split('.').join('-');

                if(data && data.rows && (data!=null)&& (data!=undefined))
                {
                    AJS.$('#tabs-'+escapedKey+'-data').html(com.wittified.fragfinder.client.renderTableData ({
                        hasPrevious: data.previous,
                        hasNext: data.next,
                        rows: data.rows,
                        headers: data.headers}));
                    for(var r=0;r<data.rows.length;r++)
                    {
                        var row= data.rows[r];
                        for(var e=0;e<row.entries.length;e++)
                        {
                            var val = row.entries[e].value;
                            AJS.$('#content-'+r+'-'+row.entries[e].name).html( val);
                        }

                    }
                    AJS.$('.wittified-webfrags-ao-previous').on('click', function()
                    {
                        window.wittified.webfrags.activeObjects.currentTablePageNum--;
                        window.wittified.webfrags.activeObjects.renderTableData();

                    })
                    AJS.$('.wittified-webfrags-ao-next').on('click', function()
                    {
                        window.wittified.webfrags.activeObjects.currentTablePageNum++;
                        window.wittified.webfrags.activeObjects.renderTableData();

                    })
                }
                else
                {
                    AJS.$('#tabs-'+escapedKey+'-data').html('<div>An error occured while looking for this table. Please file an issue with Wittified at http://wittified.atlassian.net.</div>');
                }
            });

        },
        'renderThisTable': function( table, pageNum)
        {
            this.currentTable = table;
            this.currentTablePageNum = pageNum;
            if( !this.currentTablePageNum)
            {
                this.currentTablePageNum=1;
            }
            tableName = table;

            this.currentTable = table;
            var escapedKey = table.split('.').join('-');
            var escapedPluginKey = this.currentPlugin.split('.').join('-');
            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/activeobjects/table/'+this.currentPlugin+'/'+table, function( columns)
            {
                AJS.$('#wittified-ao-contents').html( com.wittified.fragfinder.client.renderAOMeta( {escapedKey:escapedKey,columns: columns, tableName: tableName}) );
                AJS.tabs.setup();
                AJS.$('.wittified-ao-table-data').on('click', function()
                {
                    window.wittified.webfrags.activeObjects.renderTableData();
                });
            });

        }
    };
}
if( !window.wittified.webfrags.events)
{

    window.wittified.webfrags.events =
    {
        'pageNum': 1,
        'totalItems':-1,
        'filter':'',
        'orderBy': 'TIME',
        'dialog': false,
        'show': function()
        {

            this.refresh( function()
            {
                wittified.webfrags.events.renderNav();
            })

        },
        'reset': function()
        {
            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/events/reset', function(data)
            {
                AJS.$('#wittified-webfrags-events').html( com.wittified.fragfinder.client.viewEvents( {events: []}));
            });
        },
        'previousFilter': false,
        'renderNav': function()
        {

            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/events/eventNames', function(data)
            {

                var eventNames = [];

                var totalItems = 0;

                for( var i=0;i<data.length;i++)
                {
                    var splitVersion = data[i].name.split('.');
                    var escapedKey =
                    eventNames.push( { 'name': splitVersion[splitVersion.length-1], 'event': data[i].name,'escapedKey': data[i].key,"numItems": data[i].numItems})
                    totalItems = totalItems + data[i].numItems;
                }
                eventNames.unshift( {'event':'','name':'All','escapedKey':'','numItems': totalItems} );
                AJS.$('#wittified-nav').html(com.wittified.fragfinder.client.renderEventNames( {eventNames: eventNames}));

                AJS.$('.wittified-plugin-event-link').on('click', function(evt)
                {
                    evt.preventDefault();
                    evt.stopPropagation();

                    if( wittified.webfrags.events.previousFilter)
                    {
                        AJS.$('#events-'+wittified.webfrags.events.previousFilter.split('.').join('-')).removeClass('aui-nav-selected');
                    }
                    var eventKey = AJS.$(this).data('event');
                    var numItems = AJS.$(this).data('count');

                    wittified.webfrags.events.totalItems = numItems;
                    var escapedKey =eventKey.split('.').join('-');
                    wittified.webfrags.events.previousFilter = eventKey;

                    AJS.$('#events-'+escapedKey).addClass('aui-nav-selected');

                    window.wittified.webfrags.events.filter = eventKey;

                    window.wittified.webfrags.events.renderPage(1);



                });
            });

        },

        'renderPage': function(pageNum)
        {
            this.pageNum=pageNum;

            var entriesPerPage = 20;


            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/events/page/'+this.pageNum+'?sort='+this.orderBy+'&numItems=20&filter='+this.filter, function(data)
            {
                AJS.$('#wittified-webfrags-events').html( com.wittified.fragfinder.client.viewEvents( {events: data.events, hasPrevious: data.previous, hasNext: data.next}));
                AJS.$('.wittified-time-now').each( function(idx)
                {
                    var node = AJS.$(this);
                    var time = node.data('time');
                    var theMoment = moment(time*1);
                    theMoment.lang('en');
                    node.html( theMoment.fromNow());
                });
                wittified.webfrags.utils.renderPagination( wittified.webfrags.events.pageNum,  (wittified.webfrags.events.totalItems/entriesPerPage),'events' );

            });
        },
        'refresh': function(cb)
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/events/refresh',
                'type': 'POST',
                'contentType': 'application/json',
                'success': cb
                });
        }
    }
}


if( !window.wittified.webfrags.http)
{
    window.wittified.webfrags.http =
    {
        'inEnabled': true,
        'outEnabled': true,
        'pageNum': 1,
        'totalItems': 0,
        'show': function( evt)
        {

            this.refresh( function()
            {


                window.wittified.webfrags.http.renderNav();

                AJS.$('.wittified-filter-http').on('click', function(idx)
                {
                    var btn = AJS.$(this);
                    var state = btn.data('state');
                    var type = btn.data('type');
                    if( state!='false')
                    {
                        if( type=='in')
                        {
                            wittified.webfrags.http.inEnabled = false;

                        }
                        else if( type =='out')
                        {
                            wittified.webfrags.http.outEnabled = false;
                        }
                        btn.data('state', 'false');
                        console.log('setting button to false');
                        btn.attr('aria-pressed', 'false');

                    }
                    else
                    {
                        if( type=='in')
                        {
                            wittified.webfrags.http.inEnabled = true;
                        }
                        else if( type =='out')
                        {
                            wittified.webfrags.http.outEnabled = true;
                        }
                        btn.data('state', 'true');
                        console.log('setting button to true');
                        btn.attr('aria-pressed', 'true');
                    }
                    AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/http/numItems?out='+wittified.webfrags.http.outEnabled+'&in='+ wittified.webfrags.http.inEnabled+'&filter='+wittified.webfrags.http.filter, function(numItems)
                    {
                        var pageToRender = wittified.webfrags.http.pageNum;
                        wittified.webfrags.http.totalItems = numItems;
                        var lastPage = parseInt(wittified.webfrags.http.totalItems/20);
                        if(  pageToRender>lastPage)
                        {
                            pageToRender = lastPage;
                        }
                        wittified.webfrags.http.renderPage( pageToRender);
                    });

                });
            });

        },
        'reset': function()
        {
            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/http/reset', function(data)
            {
               AJS.$('#wittified-webfrags-http').html( com.wittified.fragfinder.client.viewHttp( {httpCalls: []}));
            });
        },

        'previousFilter': false,
        'renderNav': function()
        {

            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/http/serverNames', function(data)
            {

                var serverNames = [];


                var totalItems = 0;
                var selectedItems = -1;
                for( var i=0;i<data.length;i++)
                {
                    serverNames.push( { 'name': data[i].name, 'server': data[i].name,'escapedKey': data[i].key,"numItems": data[i].numItems})
                    totalItems = totalItems + data[i].numItems;
                }
                wittified.webfrags.http.totalItems = totalItems;
                serverNames.unshift( {'server':'','name':'All','escapedKey':'','numItems': totalItems})

                AJS.$('#wittified-nav').html(com.wittified.fragfinder.client.renderServerNames( {serverNames: serverNames}));

                AJS.$('.wittified-plugin-server-link').on('click', function(evt)
                {
                    evt.preventDefault();
                    evt.stopPropagation();

                    if( wittified.webfrags.http.previousFilter)
                    {
                        AJS.$('#http-'+wittified.webfrags.http.previousFilter).removeClass('aui-nav-selected');
                    }


                    var numItems = AJS.$(this).data('count');


                    wittified.webfrags.http.totalItems = numItems;

                    var escapedKey = AJS.$(this).data('escaped');
                    wittified.webfrags.http.previousFilter = escapedKey;

                    AJS.$('#http-'+escapedKey).addClass('aui-nav-selected');

                    window.wittified.webfrags.http.filter = AJS.$(this).data('server');
                    window.wittified.webfrags.http.pageNum = 1;
                    window.wittified.webfrags.http.renderPage(1);



                });
            });

        },
        'filter':'',
        'reloadCurrentPage':function()
        {
            this.renderPage( this.pageNum);
        },
        'refresh': function(cb)
        {
            AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/http/refresh',
                'type': 'POST',
                'contentType': 'application/json',
                'success': cb
                });
        },
        'renderPage': function( pageNum)
        {

            this.pageNum=pageNum;

            var entriesPerPage = 20;


            AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/webfrags/http/page/'+this.pageNum+'?out='+this.outEnabled+'&in='+ this.inEnabled+'&filter='+this.filter, function(requests)
            {
                console.log(requests);
                var data = requests.httpRequests;

                console.log(data);
                for(var d=0;d<data.length;d++)
                {
                    if( data[d].requestHeaders.length==0)
                    {
                        data[d].requestHeaders = false;
                    }
                    if( data[d].responseHeaders.length==0)
                    {
                        data[d].responseHeaders = false;
                    }
                }
                AJS.$('#wittified-webfrags-http').html( com.wittified.fragfinder.client.viewHttp( {httpCalls: data}));


                AJS.$('.httpCallHeaders').hide();

                AJS.$('.httpCall').on('click', function(event)
                {
                    event.preventDefault();
                    var id = AJS.$(this).data('id');
                    var state = AJS.$(this).data('state');
                    if(state=='expanded')
                    {
                        AJS.$('#http-request-'+id).hide();
                        AJS.$(this).data('state', 'collapsed');
                        AJS.$(this).html('<span class="aui-icon aui-icon-small aui-iconfont-collapsed"></span>');
                    }
                    else
                    {

                        AJS.$('#http-request-'+id).show();
                        AJS.$(this).data('state', 'expanded');
                        AJS.$(this).html('<span class="aui-icon aui-icon-small aui-iconfont-expanded"></span>');
                    }

                });


                AJS.$('.wittified-time-now').each( function(idx)
                {
                    var node = AJS.$(this);
                    var time = node.data('time');
                    var theMoment = moment(time*1);
                    theMoment.lang('en');
                    node.html( theMoment.fromNow());
                });

console.log('rendering pagination');
console.log(  wittified.webfrags.http.pageNum);
console.log(  wittified.webfrags.http.totalItems);
console.log(  entriesPerPage);

              wittified.webfrags.utils.renderPagination( wittified.webfrags.http.pageNum,  (wittified.webfrags.http.totalItems/entriesPerPage),'http' );




            });
        }
    }
}

