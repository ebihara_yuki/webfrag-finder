package it.com;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.contextProviders.ACHostUpdateProvider;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

/**
 * Created by daniel on 2/22/15.
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class ContextProviderTest
{
    private final WebResourceManager webResourceManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ApplicationProperties applicationProperties;

    public ContextProviderTest( final PluginSettingsFactory pluginSettingsFactory,
                                final ApplicationProperties applicationProperties,
                                final WebResourceManager webResourceManager)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.applicationProperties = applicationProperties;
        this.webResourceManager = webResourceManager;
    }


    @Test
    public void testACUpdateProvider()
    {

        ACHostUpdateProvider contextProvider = new ACHostUpdateProvider( this.webResourceManager, this.pluginSettingsFactory, this.applicationProperties);

        Assert.assertEquals("", ((String) contextProvider.getContextMap(new HashMap<String, Object>()).get("new")) );
        Assert.assertFalse(((Boolean) contextProvider.getContextMap(new HashMap<String, Object>()).get("shouldUpdateContents")));


        this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updateHost","2");

        Assert.assertEquals("2", ((String) contextProvider.getContextMap(new HashMap<String, Object>()).get("new")) );
        Assert.assertTrue(((Boolean) contextProvider.getContextMap(new HashMap<String, Object>()).get("shouldUpdateContents")));
    }


}
