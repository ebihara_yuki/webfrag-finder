package it.com;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.wittified.fragfinder.LocationEntry;
import com.wittified.fragfinder.LocationService;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Copyright 2013 Wittified, LLC
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@RunWith(AtlassianPluginsTestRunner.class)
public class LocationTest
{
	private final LocationService locationService;

	public LocationTest( final LocationService locationService)
	{
		this.locationService = locationService;
	}

	@Test
	public void testPresenceOfWebItem()
	{
		boolean hasItem = false;
		for (LocationEntry location : this.locationService.getWebItems()) {
			if ( location.getLocation().equals("system.user.options/personal") ) {
				hasItem = true;
			}
		}
		Assert.assertTrue(hasItem);

	}

	@Test
	public void testPresenceOfWebSection()
	{
		boolean hasItem = false;
		for (LocationEntry location : this.locationService.getWebSections()) {
			if ( location.getLocation().equals("admin_project_current") ) {
				hasItem = true;
			}
		}
		Assert.assertTrue(hasItem);

	}

	@Test
	public void testPresenceOfWebPanel()
	{
		boolean hasItem = false;
		for (LocationEntry location : this.locationService.getWebPanels()) {
			if ( location.getLocation().equals("tabs.admin.projectconfig.notifications") ) {
				hasItem = true;
			}
		}
		Assert.assertTrue(hasItem);

	}

}
